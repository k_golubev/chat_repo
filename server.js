const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const cons = require('consolidate');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const errorHandler = require('errorhandler');
const fs = require('fs');
const http = require('http');
const join = require('path').join;
let io = require('socket.io');
const methodOverride = require('method-override');
const path = require('path');
const passport = require('passport');
const useragent = require('express-useragent');

const models = join(__dirname, 'server/models');

const db = require('./config/db');
const logger = require('./config/logger');
const morgan = require('./config/morgan');
const cron = require('./config/cron');

const app = express();
const MongoStore = require('connect-mongo')(session);

morgan(app);

let route = null;
let server = null;
const logLevel = 'info';

logger.addConsole(logLevel);

if (app.get('env') === 'development') {
  logger.info('Development');
  app.use(errorHandler({
    dumpExceptions: true,
    showStack: true
  }));
}

const pkg = require('./package.json');

/**
 * Express
 */

const env = process.env.NODE_ENV || 'development';

const port = process.env.PORT || '3000';

app.set('port', process.env.DYNO ? 3000 : port);

app.engine('hbs', cons.handlebars);
app.set('views', path.join(__dirname, 'server/views'));
app.set('view engine', 'hbs');

//  app.use(express.static(path.join(__dirname, 'public')));

// expose package.json to views
app.use((req, res, next) => {
  res.locals.pkg = pkg;
  res.locals.env = env;
  next();
});
app.use(useragent.express());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
  res.setHeader('Cache-Control', 'no-cache');
  next();
});

// bodyParser should be above methodOverride
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors({
  origin: true,
  credentials: true
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(methodOverride((req) => {
  if (req.body && typeof req.body === 'object' && '_method' in req.body) {
    // look in urlencoded POST bodies and delete it
    const method = req.body._method;
    delete req.body._method;
    return method;
  }
}));

app.use(cookieParser());
app.use(session({
  store: new MongoStore({
    mongooseConnection: db,
    touchAfter: 24 * 3600,
    ttl: 5 * 60
  }),
  saveUninitialized: false, // don't create session until something stored
  resave: false, // don't save session if unmodified
  secret: 'chat-services'
}));

server = http.createServer(app).listen(app.get('port'), () => {
  if (process.env.DYNO) {
    console.log('Running on Heroku... ');
    fs.openSync('/tmp/app-initialized', 'w');
  }

  logger.info(`app is running on port ${app.get('port')}`);
});

io = io.listen(server);
require('./config/socket')(io);

cron.init(io);

// Bootstrap routes
require('./config/passport')(passport);

// init routes
fs.readdirSync('./server/routes').forEach((file) => {
  if (path.extname(file) === '.js') {
    route = require(`./server/routes/${file}`);
    route(app, io, passport);
  }
});

// Bootstrap models
fs.readdirSync(models)
  .filter(file => ~file.search(/^[^.].*\.js$/))
  .forEach(file => require(join(models, file)));

module.exports = app;
