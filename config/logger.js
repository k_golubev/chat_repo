const moment = require('moment');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf, colorize, prettyPrint } = format;

const combineFormat = combine(
  timestamp({ format: 'M/D/YYYY, h:mm:ss A' }),
  colorize(),
  prettyPrint(),
  printf(({ level, message, timestamp }) => {
    return `${timestamp} ${level} - ${message}`;
  })
);

class LoggingService {
  constructor () {
    const logger = createLogger({
      exitOnError: false,
      transports: []
    });

    const handleLog = (func) => {
      return (message, ...args) => {
        let realMessage = message;

        if (typeof realMessage === 'object') {
          if (realMessage.stack) realMessage = realMessage.stack;
          else realMessage = realMessage.toString();
        }

        if (args.length) {
          args.forEach(i => {
            realMessage += ` ${i}`;
          });
        }

        func(realMessage);
      };
    };

    this.info = handleLog(logger.info);
    this.error = handleLog(logger.error);
    this.warn = handleLog(logger.warn);
    this.verbose = handleLog(logger.verbose);
    this.debug = handleLog(logger.debug);
    this.silly = handleLog(logger.silly);

    this.addConsole = (logLevel) => {
      console.log('Adding logger for console, level = ' + logLevel);
      logger.add(new transports.Console({
        level: logLevel,
        handleExceptions: true,
        format: combineFormat
      }));
    };

    this.addFile = (filename, logLevel) => {
      filename += '.' + moment().format('YYYY-MM-DD');
      logger.add(new transports.File({
        level: logLevel,
        filename: filename,
        handleExceptions: true,
        json: true,
        format: combineFormat
      }));
    };
  }
}

const service = new LoggingService();

module.exports = service;
module.exports.stream = {
  write: service.info
};
