const moment = require('moment');

const logger = require('../logger');

const Conversation = require('../../server/models/conversation');

const UNACTIVE_INTERVAL = 15; // minutes

module.exports = async () => {
  try {
    const diffMinutesUpdated = moment().subtract(UNACTIVE_INTERVAL, 'minutes');

    await Conversation.deleteMany({ updatedAt: { $lte: diffMinutesUpdated } });
  } catch (err) {
    if (err) logger.error(err);
  }
};
