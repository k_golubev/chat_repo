const logger = require('../logger');

const Request = require('../../server/models/request');
const Conversation = require('../../server/models/conversation');

module.exports = async (io) => {
  try {
    const pendingRequests = await Request.find({ status: 'pending' }).populate('user');

    const pairs = [];
    const used = [];

    pendingRequests.forEach((request) => {
      if (!used.includes(request.id.toString())) {
        const { settings: { options: { self, searching } }, user } = request;

        let participantRequests = pendingRequests.filter((item) => item.user && item.user.id !== user.id);

        if (self.gender !== 'none') {
          participantRequests = participantRequests.filter((item) => item.settings.options.searching.gender === self.gender);
        }

        if (self.age !== '-1') {
          participantRequests = participantRequests.filter((item) => item.settings.options.searching.age.includes(self.age));
        }

        if (searching.gender !== 'none') {
          participantRequests = participantRequests.filter((item) => item.settings.options.self.gender === searching.gender);
        }

        if (!searching.age.some((age) => age === '-1')) {
          participantRequests = participantRequests.filter((item) => searching.age.includes(item.settings.options.self.age));
        }

        if (participantRequests.length > 0 && !used.includes(participantRequests[0].id.toString())) {
          used.push(participantRequests[0].id.toString());

          pairs.push([request.id.toString(), participantRequests[0].id.toString()]);
        }
      }
    });

    pairs.forEach(async (pair) => {
      const requests = await Request.find({ _id: { $in: pair }, status: 'pending' }).populate('user');

      if (requests.length > 0) {
        const participants = requests.map((request) => request.user);
        const requestIds = requests.map((request) => request._id);

        const states = requests.map((request) => ({ user: request.user._id, state: request.settings.state }));
        const conversation = new Conversation({ participants, states, requests });

        await conversation.save();
        await Request.updateMany({ _id: { $in: requestIds } }, { status: 'processing' });

        requests.forEach((request) => {
          console.log('start');
          io.to(`${request.user.socketId}`).emit('start-conversation', conversation);
        });
      }
    });
  } catch (err) {
    if (err) logger.error(err);
  }
};
