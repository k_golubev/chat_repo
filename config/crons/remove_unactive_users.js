const moment = require('moment');

const logger = require('../logger');

const User = require('../../server/models/user');
const Conversation = require('../../server/models/conversation');
const Request = require('../../server/models/conversation');

const UNACTIVE_INTERVAL = 5; // minutes

module.exports = async (io) => {
  try {
    const users = await User.find({ role: { $ne: 'su' } });

    users.forEach(async (user) => {
      const conversations = await Conversation.find({ participants: user._id }).populate('requests');
      const diffMinutesUpdated = moment().diff(moment(user.updatedAt), 'minutes');

      if ((!conversations.length && diffMinutesUpdated > UNACTIVE_INTERVAL)) {
        await Request.deleteMany({ user });
        await User.deleteOne({ _id: user._id });

        return;
      }

      conversations.forEach(async (conversation) => {
        const { messages } = conversation;
        const diffMinutesUpdated = moment().diff(moment(conversation.updatedAt), 'minutes');

        if (diffMinutesUpdated > UNACTIVE_INTERVAL) {
        //  if (!messages.length || diffMinutesUpdated > UNACTIVE_INTERVAL) {
          conversation.participants.forEach(async () => {
            io.to(conversation._id).emit('participan-leave-conversation', { userId: user._id, conversationId: conversation.id });
          });

          await Conversation.deleteOne({ _id: conversation._id });

          return;
        }

        const filtered = messages.filter((message) => message.from.toString() === user._id.toString());
        const lastActive = filtered.length > 0 ? moment().diff(moment(filtered[filtered.length - 1].createdAt), 'minutes') : null;

        if (lastActive && lastActive > UNACTIVE_INTERVAL) {
          io.to(conversation._id).emit('participan-leave-conversation', { userId: user._id, conversationId: conversation.id });

          await Conversation.deleteOne({ participants: user._id });
        }
      });
    });
  } catch (err) {
    if (err) logger.error(err);
  }
};
