const cron = require('cron');

const logger = require('./logger');

// Jobs
const searching = require('./crons/search');
const remove_unactive_users = require('./crons/remove_unactive_users');
const remove_unactive_conversations = require('./crons/remove_unactive_conversations');

const addCronJob = (timeRule, name, fn, io = null) => {
  if (name && fn) {
    var cronjob = new cron.CronJob(timeRule, () => {
      //  logger.info('Started ' + name);

      fn(io);
    });

    cronjob.start();
  }
};

exports.init = (io) => {
  addCronJob('* * * * * *', 'Searching participants', searching, io);

  // every minute
  addCronJob('0 */1 * * * *', 'Remove unactive users', remove_unactive_users, io);

  // every 10 minutes
  addCronJob('* */10 * * * *', 'Remove unactive conversations', remove_unactive_conversations);
};
