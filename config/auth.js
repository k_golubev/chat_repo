const config = {
  protocol: process.env.DYNO ? 'https' : 'http',
  host: process.env.HOST || 'localhost:3000'
};

module.exports = config;
