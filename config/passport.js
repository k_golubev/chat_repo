const User = require('../server/models/user');

const local = require('./passport/local');

module.exports = function (passport) {
  // serialize sessions
  passport.serializeUser((user, cb) => cb(null, user.id));
  passport.deserializeUser((id, cb) => User.load({ criteria: { _id: id } }, cb));

  // use these strategies
  passport.use(local);
};
