const logger = require('./logger');
const mongoose = require('mongoose');
const MongooseError = require('mongoose/lib/error');

const DBSERVER = 'localhost';
const DBNAME = 'chat';
const DBPORT = 27017;

const mongoUrl = process.env.MONGODB_URI || `mongodb://${DBSERVER}:${DBPORT}/${DBNAME}`;

mongoose.Promise = global.Promise;

mongoose.connection.on('open', () => {
  return logger.info('Connected to mongo server!');
});

mongoose.connection.on('error', (err) => {
  logger.warn('Could not connect to mongo server, err' + err);
  return logger.info(err.message.red);
});

mongoose.set('useFindAndModify', false);

const connectWithRetry = () => {
  var options = {
    reconnectTries: 30,
    socketTimeoutMS: 3e4,
    keepAlive: true,
    useCreateIndex: true,
    useNewUrlParser: true
  };

  return mongoose.connect(mongoUrl, options, (err) => {
    if (err) {
      logger.warn('Failed to connect to mongo on startup - retrying in 1 sec', err);
      setTimeout(connectWithRetry, 1e3);
    }
  });
};

connectWithRetry();

MongooseError.ValidationError.prototype.toString = () => {
  var wrongs = [];
  Object.keys(this.errors).forEach(function (key) {
    var error = this.errors[key];
    wrongs.push(key + ': "' + error.value + '"');
  }.bind(this));
  return this.message + ' - ' + wrongs.join(', ');
};

module.exports = mongoose.connection;
