const User = require('../server/models/user');
const Conversation = require('../server/models/conversation');

module.exports = async (io) => {
  if (process.env.NODE_ENV === 'development') {
    //  await User.deleteMany({});
    //  await Conversation.deleteMany({});
    //  await Request.deleteMany({});
  }

  io.on('connection', async (socket) => {
    socket.on('join', async (data = null) => {
      let user = null;

      if (data && data.userId) {
        await User.findByIdAndUpdate(data.userId, { socketId: socket.id });

        user = await User.findOne({ _id: data.userId });
      }

      if (!user) return;

      const conversations = await Conversation.find({ participants: user._id });

      conversations.forEach((conversation) => {
        socket.join(conversation._id);
      });

      socket.emit('connected', { user });
    });

    socket.on('leave-conversation', async (userId) => {
      const conversation = await Conversation.findOne({ participants: userId });

      if (conversation) {
        socket.to(conversation._id).emit('participan-leave-conversation', { userId, conversationId: conversation._id });

        await Conversation.deleteOne({ _id: conversation._id });
      }
    });

    socket.on('change-participant', async (userId) => {
      const conversation = await Conversation.findOne({ participants: userId });

      if (conversation) {
        socket.to(conversation._id).emit('participan-leave-conversation', { userId: userId, conversationId: conversation._id });

        await Conversation.deleteOne({ _id: conversation._id });
      }
    });

    socket.on('typing', (data) => {
      const { typing, conversation, user } = data;

      if (conversation) socket.to(conversation).emit('typing', { typing, user });
    });

    socket.on('disconnect', async () => {
      const user = await User.findOne({ socketId: socket.id });

      if (user && (user.isSearching || user.isReady)) await User.deleteOne({ _id: user._id });
    });
  });
};
