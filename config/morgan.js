const morgan = require('morgan');

const logger = require('./logger');
const authConfig = require('./auth');

const minResponseTime = authConfig.minResponseTime;

module.exports = function (app) {
  app.use(
    morgan(':method [:status] :url session=:session', {
      stream: logger.stream
    }));

  morgan.token('response-time', function getResponseTime (req, res, digits) {
    // copy from https://github.com/expressjs/morgan/blob/e17e76ff376e6951c05d0a8c5bb89f091ea0505b/index.js#L228
    if (!req._startAt || !res._startAt) {
      return;
    }

    const ms = (res._startAt[0] - req._startAt[0]) * 1e3 +
      (res._startAt[1] - req._startAt[1]) * 1e-6;

    (ms > minResponseTime) && !req._slowResponse && logger.warn('SLOW RESPONSE', ms, req.url);

    return ms.toFixed(digits === undefined ? 3 : digits);
  });

  morgan.token('requestId', function getRequestId (req) {
    return req._requestId;
  });

  morgan.token('status', function getStatus (req, res) {
    return res.statusCode;
  });

  morgan.token('session', function getSession (req) {
    return req && req.session ? req.session.id : 'null';
  });
};
