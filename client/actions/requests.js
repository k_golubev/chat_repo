import {
  SET_REQUESTS,
  SET_ACTIVE_REQUEST
} from 'Types/requests';

import { getRequests, getActiveRequest } from 'Selectors/requests';

import { deleteRequest as deleteRequestRest } from 'Rest/requests';

export const setRequests = (requests) => ({ type: SET_REQUESTS, payload: requests });

export const setActiveRequest = (request) => ({ type: SET_ACTIVE_REQUEST, payload: request });

export const addRequest = (request) => (dispatch, getState) => {
  const active = getActiveRequest(getState());
  const requests = getRequests(getState());

  if (!active) dispatch(setActiveRequest(request));

  const nextRequests = [...requests, request];

  dispatch(setRequests(nextRequests));
};

export const deleteRequest = (requestId) => (dispatch, getState) => {
  const requests = getRequests(getState());
  const active = getActiveRequest(getState());

  return deleteRequestRest(requestId).then(() => {
    const nextRequests = requests.filter((item) => item._id !== requestId);

    if (active && active.id === requestId) dispatch(setActiveRequest(null));

    dispatch(setRequests(nextRequests));
  });
};

export const updateRequest = (value) => (dispatch, getState) => {
  const requests = getRequests(getState());
  const activeRequest = getActiveRequest(getState());

  const nextRequests = requests.map((item) => {
    if (item._id !== value._id) return item;

    return value;
  });

  dispatch(setRequests(nextRequests));
  if (value._id === activeRequest._id) dispatch(setActiveRequest(value));
};
