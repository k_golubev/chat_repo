import {
  SET_USER,
  SET_ONLINE
} from 'Types/current-user';

export const setUser = (value) => ({ type: SET_USER, payload: value });

export const setOnline = (value) => ({ type: SET_ONLINE, payload: value });
