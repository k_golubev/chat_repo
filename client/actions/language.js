import {
  SET_LANGUAGES
} from 'Types/language';

import {
  getLanguages as getLanguagesRest
} from 'Rest/language';

export const getLanguages = () => (dispatch) => {
  return getLanguagesRest().then((languages) => {
    dispatch({ type: SET_LANGUAGES, payload: languages });
  });
};
