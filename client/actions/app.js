import {
  SET_IS_EXIT,
  SET_IS_ACCEPT,
  SET_SETTINGS_OPEN,
  SET_IS_REQUEST_FORM
} from 'Types/app';

export const setIsExit = (value) => ({ type: SET_IS_EXIT, payload: value });
export const setIsAccept = (value) => ({ type: SET_IS_ACCEPT, payload: value });
export const setIsSettingsOpen = (value) => ({ type: SET_SETTINGS_OPEN, payload: value });
export const setIsRequestForm = (value) => ({ type: SET_IS_REQUEST_FORM, payload: value });
