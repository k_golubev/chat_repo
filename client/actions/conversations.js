import {
  SET_ACTIVE_CONVERSATION,
  SET_CONVERSATIONS
} from 'Types/conversations';

import { getConversations, getActiveConversation } from 'Selectors/conversations';

export const setActiveConversation = (value) => ({ type: SET_ACTIVE_CONVERSATION, payload: value });

export const setConversations = (value) => ({ type: SET_CONVERSATIONS, payload: value });

export const deleteConversation = (value) => (dispatch, getState) => {
  const conversations = getConversations(getState());
  const activeConversation = getActiveConversation(getState());

  const nextConversations = conversations.filter((item) => item._id !== value);

  dispatch(setConversations(nextConversations));

  if (activeConversation._id === value) dispatch(setActiveConversation(null));
};

export const updateConversation = (value) => (dispatch, getState) => {
  const conversations = getConversations(getState());

  const nextConversations = conversations.map((item) => {
    if (item._id !== value._id) return item;

    const conversation = {
      ...value,
      unread: true
    };

    return conversation;
  });

  dispatch(setActiveConversation(value));
  dispatch(setConversations(nextConversations));
};
