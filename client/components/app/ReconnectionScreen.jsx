import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import { connect } from 'react-redux';

import { Translation } from 'react-i18next';

import withStyles from '@material-ui/core/styles/withStyles';

import { getOnline, getCurrentUser } from 'Selectors/current-user';
import { setOnline } from 'Actions/current-user';

import styles from 'Styles/app/reconnection-screen';

import socket from 'Modules/socket';

class ReconnectionScreen extends React.Component {
  constructor (props) {
    super(props);

    socket.on('reconnect_error', () => {
      this.handleReConnectError();
    });

    socket.on('reconnect', () => {
      this.handleReConnect();
    });

    this.handleReConnect = this.handleReConnect.bind(this);
    this.handleReConnectError = this.handleReConnectError.bind(this);
  }

  handleReConnect () {
    const { dispatch, user } = this.props;

    if (user) {
      socket.emit('join', { userId: user._id });

      dispatch(setOnline(true));
    }
  }

  handleReConnectError () {
    const { dispatch, isOnline } = this.props;

    if (isOnline) dispatch(setOnline(false));
  }

  render () {
    const { classes, isOnline } = this.props;

    if (isOnline) return null;

    return (
      <div className={classes.container}>
        <Translation>
          {
            (t) => t('reconnection_screen.service_unavailable')
          }
        </Translation>
      </div>
    );
  }
};

ReconnectionScreen.propTypes = {
  classes: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  isOnline: PropTypes.bool.isRequired,
  user: PropTypes.object
};

ReconnectionScreen.defaultProps = {
  user: null
};

const mapStateToProps = state => ({
  isOnline: getOnline(state),
  user: getCurrentUser(state)
});

export default compose(
  withStyles(styles),
  connect(mapStateToProps)
)(ReconnectionScreen);
