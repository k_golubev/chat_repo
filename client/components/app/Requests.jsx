import React from 'react';
import PropTypes from 'prop-types';
import compose from 'recompose/compose';
import { connect } from 'react-redux';
import classnames from 'classnames';

import moment from 'moment';

import capitalize from 'lodash/capitalize';

import { withTranslation } from 'react-i18next';
import withStyles from '@material-ui/core/styles/withStyles';

import { List } from 'react-virtualized';

import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

import WatchIcon from '@material-ui/icons/WatchLater';
import MessageIcon from '@material-ui/icons/Message';

import SelectFilter from 'Components/app/requests/SelectFilter';

import { setActiveRequest, deleteRequest, setRequests } from 'Actions/requests';
import { setActiveConversation } from 'Actions/conversations';

import { getCurrentUser } from 'Selectors/current-user';
import { getActiveRequest, getRequests } from 'Selectors/requests';
import { getConversations } from 'Selectors/conversations';

import { parseRequestsCSV, removeAllRequest } from 'Rest/requests';

import styles from 'Styles/app/requests/requests';

const HEIGHT = 600;
const ROW_HEIGHT = 76;
const WIDTH = 928;

const ownFilterData = (t) => {
  return [{
    value: -1,
    label: t('own_filter.all')
  }, {
    value: 1,
    label: t('own_filter.own')
  }, {
    value: 0,
    label: t('own_filter.not_own')
  }];
};

const statusFilterData = (t) => {
  return [{
    value: 'all',
    label: t('status_filter.all')
  }, {
    value: 'pending',
    label: t('status_filter.pending')
  }, {
    value: 'processing',
    label: t('status_filter.processing')
  }];
};

class Requests extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      filter: {
        own: -1,
        status: 'all',
        state: ''
      }
    };

    this.ref = React.createRef();

    this.initRef = this.initRef.bind(this);

    this.getConversationByRequestId = this.getConversationByRequestId.bind(this);

    this.rowRenderer = this.rowRenderer.bind(this);

    this.handleSetActiveRequest = this.handleSetActiveRequest.bind(this);
    this.handleGoToChat = this.handleGoToChat.bind(this);
    this.handleDeleteRequest = this.handleDeleteRequest.bind(this);
    this.handleChangeFilter = this.handleChangeFilter.bind(this);
    this.handleChangeCSVFile = this.handleChangeCSVFile.bind(this);
    this.handleRemoveAll = this.handleRemoveAll.bind(this);
  }

  componentDidUpdate () {
    const { user, router } = this.props;

    this.ref.forceUpdateGrid();

    if (user && !user.isSU) router.history.push('/');
  }

  get data () {
    const { filter } = this.state;
    const { user, requests } = this.props;

    let filtredRequests = filter.state.length ? requests.filter((request) => request.settings.state.toLowerCase().search(filter.state.toLowerCase()) > -1) : requests;

    filtredRequests = filter.status !== 'all' ? filtredRequests.filter((request) => request[`is${capitalize(filter.status)}`]) : filtredRequests;

    switch (filter.own) {
      case 0:
        filtredRequests = filtredRequests.filter((request) => request.user !== user.id);
        break;
      case 1:
        filtredRequests = filtredRequests.filter((request) => request.user === user.id);
        break;
    }

    return filtredRequests;
  }

  getConversationByRequestId (requestId) {
    const { conversations } = this.props;

    return conversations.find((item) => item.requests.includes(requestId));
  }

  goToChat (requestId) {
    const { classes, activeRequest, t } = this.props;

    if (!activeRequest || activeRequest.id !== requestId) return null;

    return (
      <Typography
        component="span"
        color="primary"
        classes={{ root: classnames(classes.metaDataItem, classes.link) }}
        onClick={() => this.handleGoToChat(requestId)}
      >
        {t('requests.go_to_chat')}
      </Typography>
    );
  }

  makeActive (request) {
    const {
      classes,
      t,
      activeRequest,
      user
    } = this.props;

    if ((user.id !== request.user) || (activeRequest && activeRequest.id === request.id)) return null;

    return (
      <Typography
        component="span"
        color="primary"
        classes={{ root: classnames(classes.metaDataItem, classes.link) }}
        onClick={() => this.handleSetActiveRequest(request.id)}
      >
        {t('requests.set_active')}
      </Typography>
    );
  }

  deleteRequest (requestId) {
    const { classes, t } = this.props;

    return (
      <Typography
        component="span"
        color="primary"
        classes={{ root: classnames(classes.metaDataItem, classes.link) }}
        onClick={() => this.handleDeleteRequest(requestId)}
      >
        {t('requests.delete')}
      </Typography>
    );
  }

  initRef (ref) {
    this.ref = ref;
  }

  rowRenderer (props) {
    const { classes } = this.props;

    const { index, key, style } = props;

    const requestId = this.data[index].id;

    const conversation = this.getConversationByRequestId(requestId);

    const stateClasses = [classes.request];

    if (conversation && conversation.unread) stateClasses.push(classes.bold);

    return (
      <ListItem key={key} style={style} disableGutters classes={{ root: classes.listItem }}>
        <ListItemText
          primary={this.data[index].settings.state}
          primaryTypographyProps={{
            noWrap: true,
            classes: { root: classnames(stateClasses) }
          }}
          secondaryTypographyProps={{
            classes: { root: classes.metaData }
          }}
          secondary={(
            <>
              <Typography
                component="span"
                classes={{ root: classes.metaDataItem }}
              >
                {capitalize(this.data[index].status)}
              </Typography>
              <Typography
                component="span"
                classes={{ root: classes.metaDataItem }}
              >
                <WatchIcon />
                {moment().diff(moment(this.data[index].createdAt), 'minutes')}
              </Typography>

              {conversation && (
                <Typography
                  component="span"
                  classes={{ root: classes.metaDataItem }}
                >
                  <MessageIcon />
                  {conversation.messages.length}
                </Typography>
              )}
              {this.makeActive(this.data[index])}
              {this.goToChat(requestId)}
              {this.deleteRequest(requestId)}
            </>
          )}
        />
      </ListItem>
    );
  };

  handleDeleteRequest (requestId) {
    const { dispatch, requests } = this.props;

    const request = requests.find((item) => item.id === requestId);

    if (request) dispatch(deleteRequest(request._id));
  }

  handleSetActiveRequest (requestId) {
    const { conversations, dispatch, requests } = this.props;

    const request = requests.find((item) => item.id === requestId);
    const conversation = conversations.find((item) => item.requests.includes(requestId));

    dispatch(setActiveRequest(request));
    dispatch(setActiveConversation(conversation));
  }

  handleGoToChat (requestId) {
    const { dispatch } = this.props;

    const conversation = this.getConversationByRequestId(requestId);

    if (conversation) {
      const nextConversation = {
        ...conversation,
        unread: false
      };

      dispatch(setActiveConversation(nextConversation));
    }

    const { router } = this.props;

    router.history.push('/');
  }

  handleChangeFilter (e) {
    const { target: { value, name } } = e;
    const { filter } = this.state;

    filter[name] = value;

    this.setState({ filter });
  }

  handleChangeCSVFile (event) {
    const { activeRequest, dispatch, requests } = this.props;
    const { target: { id, files } } = event;

    const isFiles = files && files.length;

    if (!isFiles) return;

    const formData = new FormData();

    formData.append('csv', files[0]);

    parseRequestsCSV({}, {
      body: formData,
      headers: {}
    }).then((response) => {
      const nextRequests = [...requests, ...response];

      dispatch(setRequests(nextRequests));

      if (!activeRequest) dispatch(setActiveRequest(nextRequests[0]));

      document.getElementById(id).value = '';
    });
  }

  handleRemoveAll () {
    const { dispatch } = this.props;

    removeAllRequest().then(() => {
      console.log('all requests was removed');

      dispatch(setRequests([]));
    });
  }

  render () {
    const { classes, t } = this.props;
    const { filter } = this.state;

    return (
      <Container maxWidth="md" classes={{ root: classes.root }}>
        <Grid container item classes={{ container: classes.filters }}>
          <SelectFilter
            items={ownFilterData(t)}
            name="own"
            onChange={this.handleChangeFilter}
            value={filter.own}
            label={t('own_filter.label')}
          />
          <SelectFilter
            items={statusFilterData(t)}
            name="status"
            onChange={this.handleChangeFilter}
            value={filter.status}
            label={t('status_filter.label')}
          />
          <div className={classes.grow} />
          <label htmlFor="CSVfile">
            <input
              className={classes.fileInputHidden}
              id="CSVfile"
              name="CSVfile"
              type="file"
              onChange={this.handleChangeCSVFile}
              accept=".csv"
            />
            <Button
              component="span"
              variant="outlined"
              className={classes.uploadCsv}
            >
              {t('upload')}
            </Button>
          </label>

          <Button
            component="span"
            variant="outlined"
            onClick={this.handleRemoveAll}
          >
            {t('remove_all_requests')}
          </Button>
        </Grid>
        <Grid container item classes={{ container: classes.search }}>
          <TextField
            fullWidth
            value={filter.state}
            variant="outlined"
            label={t('search_request')}
            name="state"
            onChange={this.handleChangeFilter}
          />
        </Grid>
        <Grid container classes={{ container: classes.container }}>
          <Grid item>
            <List
              ref={this.initRef}
              rowCount={this.data.length}
              width={WIDTH}
              height={HEIGHT}
              rowHeight={ROW_HEIGHT}
              rowRenderer={this.rowRenderer}
              className={classes.virtualizedList}
            />
          </Grid>
        </Grid>
      </Container>
    );
  }
};

Requests.propTypes = {
  classes: PropTypes.object.isRequired,
  conversations: PropTypes.arrayOf(PropTypes.object).isRequired,
  dispatch: PropTypes.func.isRequired,
  activeRequest: PropTypes.object,
  requests: PropTypes.arrayOf(PropTypes.object).isRequired,
  router: PropTypes.object.isRequired,
  user: PropTypes.object,
  t: PropTypes.func.isRequired
};

Requests.defaultProps = {
  activeRequest: null,
  user: null
};

const mapStateToProps = state => ({
  conversations: getConversations(state),
  activeRequest: getActiveRequest(state),
  requests: getRequests(state),
  user: getCurrentUser(state)
});

export default compose(
  withStyles(styles),
  withTranslation(),
  connect(mapStateToProps)
)(Requests);
