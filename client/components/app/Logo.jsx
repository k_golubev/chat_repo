import React from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';

import Grid from '@material-ui/core/Grid';

import styles from 'Styles/app/logo';

function TemplateFuncComponent (props) {
  const { classes } = props;

  return (
    <Grid container item classes={{ container: classes.root }} justify="center">
      <img src="/images/logos/logo.png" alt="Chatface" />
    </Grid>
  );
};

TemplateFuncComponent.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(TemplateFuncComponent);
