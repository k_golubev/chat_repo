import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';

import withStyles from '@material-ui/core/styles/withStyles';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

import { login } from 'Rest/auth';
import { getSelfUser } from 'Rest/current-user';

import { setUser } from 'Actions/current-user';

import styles from 'Styles/app/login-page';

function LoginPage (props) {
  const { classes, router } = props;

  const dispatch = useDispatch();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const { t } = useTranslation();

  const handleSubmit = () => {
    login({ email, password }).then((response) => {
      getSelfUser().then((user) => {
        dispatch(setUser(user));

        const { redirectUrl } = response;

        if (redirectUrl) router.history.push(response.redirectUrl);
      });
    });
  };

  const handleInputKeyUp = (event) => {
    if (event.keyCode === 13) handleSubmit();
  };

  return (
    <Dialog open>
      <DialogContent>
        <Grid container>
          <Grid item xs={12}>
            <TextField
              fullWidth
              autoFocus
              name="email"
              value={email}
              label={t('email_label')}
              margin="dense"
              onChange={(e) => setEmail(e.target.value)}
              onKeyUp={handleInputKeyUp}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              fullWidth
              name="password"
              value={password}
              label={t('password_label')}
              type="password"
              onChange={(e) => setPassword(e.target.value)}
              onKeyUp={handleInputKeyUp}
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions classes={{ root: classes.actions }}>
        <Button
          variant="contained"
          type="submit"
          color="primary"
          disabled={!email || !password}
          onClick={handleSubmit}
        >
          {t('login')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

LoginPage.propTypes = {
  classes: PropTypes.object.isRequired,
  router: PropTypes.object.isRequired
};

export default withStyles(styles)(LoginPage);
