import React, { useEffect, useState, Suspense } from 'react';
import { useDispatch } from 'react-redux';

import Basil from 'basil.js';

import Grid from '@material-ui/core/Grid';

import OptionSettings from 'Components/app/OptionSettings';
import RoomState from 'Components/app/RoomState';
import StartButton from 'Components/app/StartButton';

import { setUser } from 'Actions/current-user';
import { setActiveRequest } from 'Actions/requests';

import { createRequest } from 'Rest/requests';

import socket from 'Modules/socket';
import ga from 'Modules/ga';
import data from 'Data/request';

const basil = new Basil();

function StartScreen () {
  const dispatch = useDispatch();

  const [settings, setSettings] = useState(basil.get('settings') || data.initSettings);

  useEffect(() => {
    basil.set('settings', settings);
  });

  const onChangeOptions = (options) => {
    setSettings(oldValues => ({
      ...oldValues,
      options
    }));
  };

  const onChangeState = (state) => {
    setSettings(oldValues => ({
      ...oldValues,
      state
    }));
  };

  const handleStart = () => {
    ga.event({
      category: 'Start screen',
      action: 'Start searching',
      label: settings.state && settings.state.length ? settings.state : 'Empty'
    });

    createRequest(settings).then((response) => {
      const { user, request } = response;

      dispatch(setActiveRequest(request));

      if (!user.sokectId) {
        socket.emit('join', { userId: user._id });

        return;
      }

      dispatch(setUser(user));
    });
  };

  return (
    <Grid container alignItems="flex-start">
      <Suspense fallback={<div />}>
        <RoomState state={settings.state} onChange={onChangeState}/>
      </Suspense>

      <Suspense fallback={<div />}>
        <OptionSettings options={settings.options} onChange={onChangeOptions} />
      </Suspense>

      <Suspense fallback={<div />}>
        <StartButton onStart={handleStart} />
      </Suspense>
    </Grid>
  );
}

export default StartScreen;
