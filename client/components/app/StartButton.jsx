import React from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'react-i18next';

import withStyles from '@material-ui/core/styles/withStyles';

import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';

import styles from 'Styles/app/start-button';

function StartButton (props) {
  const { classes, onStart } = props;
  const { t } = useTranslation();

  return (
    <Grid container item xs={12} classes={{ item: classes.root }} justify="center">
      <Fab
        variant="extended"
        color="primary"
        aria-label="add"
        classes={{ root: classes.fab }}
        onClick={onStart}
      >
        {t('start_button')}
      </Fab>
    </Grid>
  );
};

StartButton.propTypes = {
  classes: PropTypes.object.isRequired,
  onStart: PropTypes.func.isRequired
};

export default withStyles(styles)(StartButton);
