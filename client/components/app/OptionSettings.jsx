import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import withStyles from '@material-ui/core/styles/withStyles';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import UserFilter from 'Components/app/UserFilter';

import CloseIcon from '@material-ui/icons/KeyboardArrowDown';
import OpenIcon from '@material-ui/icons/KeyboardArrowUp';

import { setIsSettingsOpen } from 'Actions/app';

import { getIsSettingsOpen } from 'Selectors/app';

import styles from 'Styles/app/option-settings';

function OptionSettings (props) {
  const {
    classes,
    options,
    onChange,
    open
  } = props;
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const isSettingsOpen = open || useSelector((state) => getIsSettingsOpen(state));

  const onToggle = () => {
    dispatch(setIsSettingsOpen(!isSettingsOpen));
  };

  const onChangeData = (data, field) => {
    const nextOptions = { ...options };

    nextOptions[field] = data;

    onChange(nextOptions);
  };

  return (
    <Grid container classes={{ root: classes.root }} spacing={2}>
      <Typography component="span" className={classes.toggle} onClick={onToggle}>
        {t('settings.title')}

        {!open && (
          <>
            {isSettingsOpen ? (
              <OpenIcon className={classes.icon} />
            ) : (
              <CloseIcon className={classes.icon} />
            )}
          </>
        )}
      </Typography>

      {isSettingsOpen && (
        <Grid container item xs={12} classes={{ root: classes.optionsRoot }}>
          <UserFilter label={t('settings.you')} onChange={(data) => onChangeData(data, 'self')} data={options.self} />

          <UserFilter label={t('settings.interlocutor')} onChange={(data) => onChangeData(data, 'searching')} data={options.searching} />
        </Grid>
      )}
    </Grid>
  );
};

OptionSettings.propTypes = {
  classes: PropTypes.object.isRequired,
  options: PropTypes.shape({
    self: PropTypes.object,
    searching: PropTypes.object
  }).isRequired,
  onChange: PropTypes.func.isRequired,
  open: PropTypes.bool
};

OptionSettings.defaultProps = {
  open: false
};

export default withStyles(styles)(OptionSettings);
