import React from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import styles from 'Styles/app/requests/select-filters';

function SelectFilter (props) {
  const {
    classes,
    onChange,
    name,
    value,
    items,
    label
  } = props;

  return (
    <FormControl classes={{ root: classes.root }}>
      <InputLabel htmlFor={name}>{label}</InputLabel>
      <Select
        value={value}
        onChange={onChange}
        inputProps={{ name }}
      >
        {items.map((item) => (
          <MenuItem key={`item-${item.label}`} value={item.value}>{item.label}</MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

SelectFilter.propTypes = {
  classes: PropTypes.object.isRequired,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  onChange: PropTypes.func.isRequired
};

export default withStyles(styles)(SelectFilter);
