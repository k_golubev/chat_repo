import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

import withStyles from '@material-ui/core/styles/withStyles';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import RoomState from 'Components/app/RoomState';
import OptionSettings from 'Components/app/OptionSettings';

import { setIsRequestForm } from 'Actions/app';
import { setActiveRequest, addRequest } from 'Actions/requests';

import { getIsRequestForm } from 'Selectors/app';
import { getActiveRequest } from 'Selectors/requests';

import { createRequest } from 'Rest/requests';

import styles from 'Styles/app/requests/request-form';
import socket from 'Modules/socket';
import data from 'Data/request';

function RequestForm (props) {
  const { classes } = props;

  const [settings, setSettings] = useState(data.initSettings);
  const [processing, setProcessing] = useState(false);

  const { t } = useTranslation();
  const dispatch = useDispatch();
  const open = useSelector((state) => getIsRequestForm(state));
  const activeRequest = useSelector((state) => getActiveRequest(state));

  const handleClose = () => {
    dispatch(setIsRequestForm(false));
  };

  const handleChangeOptions = (options) => {
    setSettings(oldValues => ({
      ...oldValues,
      options
    }));
  };

  const handeChageState = (state) => {
    setSettings(oldValues => ({
      ...oldValues,
      state
    }));
  };

  const handleSubmit = () => {
    setProcessing(true);

    createRequest(settings).then((response) => {
      setProcessing(false);

      const { user, request } = response;

      if (!activeRequest) {
        dispatch(setActiveRequest(request));

        if (!user.sokectId) {
          socket.emit('join', { userId: user._id });
        }
      }

      data.initSettings.options.searching.age = [];

      setSettings(data.initSettings);

      dispatch(addRequest(request));
    });
  };

  return (
    <Dialog
      open={open}
      disableBackdropClick
      disableEscapeKeyDown
      classes={{ paper: classes.root }}
      onClose={handleClose}
    >
      <DialogTitle>
        {t('request_form.title')}
      </DialogTitle>
      <DialogContent classes={{ root: classes.content }}>
        <RoomState
          state={settings.state}
          onChange={handeChageState}
          isHelperText={false}
        />
        <OptionSettings
          options={settings.options}
          onChange={handleChangeOptions}
          open
        />
      </DialogContent>

      <DialogActions>
        <Button color="primary" onClick={handleClose}>
          {t('close')}
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={handleSubmit}
          disabled={processing}
        >
          {t('create')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

RequestForm.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(RequestForm);
