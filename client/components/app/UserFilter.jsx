import React from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'react-i18next';

import withStyles from '@material-ui/core/styles/withStyles';

import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import FormLabel from '@material-ui/core/FormLabel';
import Grid from '@material-ui/core/Grid';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';

import styles from 'Styles/app/user-filter';

function UserFilter (props) {
  const {
    classes,
    data,
    label,
    onChange
  } = props;

  const { t } = useTranslation();

  const isMulti = Array.isArray(data.age);

  const handleChange = (event) => {
    const nextValue = {
      ...data,
      [event.target.name]: event.target.value
    };

    onChange(nextValue);
  };

  const handleChangeSearchingAges = name => event => {
    let age = data.age;

    const value = name.toString();

    if (event.target.checked) {
      age.push(value);
    } else {
      age = age.filter((item) => item !== value);
    }

    const nextValue = {
      ...data,
      age
    };

    onChange(nextValue);
  };

  const ages = [{
    value: '0',
    label: t('user_filter.age', { context: '0' })
  }, {
    value: '1',
    label: t('user_filter.age', { context: '1' })
  }, {
    value: '2',
    label: t('user_filter.age', { context: '2' })
  }, {
    value: '3',
    label: t('user_filter.age', { context: '3' })
  }];

  const genders = [{
    value: 'none',
    label: t('user_filter.gender', { context: 'none' })
  }, {
    value: 'female',
    label: t('user_filter.gender', { context: 'female' })
  }, {
    value: 'male',
    label: t('user_filter.gender', { context: 'male' })
  }];

  return (
    <Grid
      container
      item
      xs={6}
      justify="flex-start"
      classes={{ root: classes.root }}
      spacing={4}
    >
      <Grid item xs={12} classes={{ item: classes.item }}>
        <Typography component="span" className={classes.label}>
          {label}
        </Typography>

        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="gender">{`${t('user_filter.gender')}:`}</InputLabel>
          <Select
            value={data.gender}
            onChange={handleChange}
            inputProps={{ name: 'gender' }}
          >
            {genders.map((item) => <MenuItem key={`gender-${item.value}`} value={item.value}>{item.label}</MenuItem>)}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <FormLabel className={classes.legend}>{`${t('user_filter.age')}:`}</FormLabel>

          {!isMulti ? (
            <RadioGroup
              aria-label="age"
              name="age"
              value={data.age}
              onChange={handleChange}
            >
              {ages.map((item) => <FormControlLabel key={`item-${item.value}`} value={item.value} control={<Radio color="primary" />} label={item.label} />)}
            </RadioGroup>
          ) : (
            <FormGroup>
              {ages.map((age) => {
                const checked = data.age.some((item) => item === age.value);

                return (
                  <FormControlLabel
                    key={`item-${age.value}`}
                    control={<Checkbox checked={checked} onChange={handleChangeSearchingAges(age.value)} value={age.value} color="primary" />}
                    label={age.label}
                  />
                );
              })}
            </FormGroup>
          )}
        </FormControl>
      </Grid>
    </Grid>
  );
};

UserFilter.propTypes = {
  classes: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  data: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired
};

export default withStyles(styles)(UserFilter);
