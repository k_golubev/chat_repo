import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import withStyles from '@material-ui/core/styles/withStyles';

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import History from 'Components/app/chat/History';
import InputField from 'Components/app/chat/InputField';
import WaitingParticipants from 'Components/app/chat/WatingParticipants';
import Typing from 'Components/app/chat/Typing';

import { getCurrentUser } from 'Selectors/current-user';
import { getActiveConversation } from 'Selectors/conversations';
import { getActiveRequest } from 'Selectors/requests';

import { onSendMessage as onSendMessageRest } from 'Rest/app';

import styles from 'Styles/app/chat';

const isWaiting = (request) => {
  return request && request.isPending;
};

function Chat (props) {
  const { classes, typing } = props;

  const user = useSelector((state) => getCurrentUser(state));
  const conversation = useSelector((state) => getActiveConversation(state));
  const request = useSelector((state) => getActiveRequest(state));

  const isWaitingParticipant = useMemo(() => isWaiting(request), [request]);

  const onSendMessage = (body) => {
    const message = {
      from: user._id,
      createdAt: new Date(),
      body
    };

    const messageToRest = {
      message,
      conversation: conversation._id
    };

    onSendMessageRest(messageToRest);
  };

  return (
    <Grid container classes={{ root: classes.root }}>
      <Grid item xs={12}>
        <Paper classes={{ root: classes.paper }}>
          <Grid container classes={{ container: classes.wrapper }}>
            <History />

            <InputField onSendMessage={onSendMessage} />

            {isWaitingParticipant && <WaitingParticipants />}

            {typing && <Typing />}
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
};

Chat.propTypes = {
  classes: PropTypes.object.isRequired,
  typing: PropTypes.bool.isRequired
};

export default withStyles(styles)(Chat);
