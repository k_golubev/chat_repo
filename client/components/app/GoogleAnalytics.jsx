import { useEffect } from 'react';

import ga from 'Modules/ga';

function GoogleAnalytics () {
  useEffect(() => {
    const { location: { pathname, search } } = window;

    ga && ga.pageview(`${pathname}${search}`);
  });

  return null;
};

export default GoogleAnalytics;
