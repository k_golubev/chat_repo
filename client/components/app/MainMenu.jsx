import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

import { Link } from 'react-router-dom';

import withStyles from '@material-ui/core/styles/withStyles';

import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import RequestsIcon from '@material-ui/icons/List';
import LogoutIcon from '@material-ui/icons/ExitToApp';

import styles from 'Styles/app/main-menu';

function MainMenu (props) {
  const { classes, open, onClose } = props;

  const { t } = useTranslation();

  const logout = () => {
    location.href = '/logout';
  };

  return (
    <Drawer open={open} classes={{ paperAnchorLeft: classes.paperAnchorLeft }} onClose={onClose}>
      <div className={classes.logo}>
        <img src="/images/logos/logo.png" alt="Chatface" />
      </div>
      <List classes={{ root: classes.list }}>
        <ListItem button component={Link} to='/requests' onClick={onClose}>
          <ListItemIcon>
            <RequestsIcon />
          </ListItemIcon>
          <ListItemText>
            {t('main_menu.requests')}
          </ListItemText>
        </ListItem>
        <Divider />
        <ListItem button onClick={logout}>
          <ListItemIcon>
            <LogoutIcon />
          </ListItemIcon>
          <ListItemText>
            {t('main_menu.logout')}
          </ListItemText>
        </ListItem>
      </List>
    </Drawer>
  );
};

MainMenu.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired
};

export default withStyles(styles)(MainMenu);
