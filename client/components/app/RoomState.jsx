import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

import withStyles from '@material-ui/core/styles/withStyles';

import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

import styles from 'Styles/app/room-state';

function RoomState (props) {
  const {
    classes,
    onChange,
    state,
    isHelperText
  } = props;
  const { t } = useTranslation();

  const helperText = useMemo(() => {
    return isHelperText ? t('roomstate.placeholder') : ''
  }, [isHelperText]);

  return (
    <Grid item xs={12} classes={{ item: classes.root }}>
      <TextField
        fullWidth
        label={t('roomstate.label')}
        placeholder={t('roomstate.placeholder')}
        helperText={helperText}
        value={state}
        className={classes.textField}
        margin="normal"
        variant="outlined"
        onChange={(e) => onChange(e.target.value)}
      />
    </Grid>
  );
};

RoomState.propTypes = {
  classes: PropTypes.object.isRequired,
  state: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  isHelperText: PropTypes.bool
};

RoomState.defaultProps = {
  isHelperText: true
};

export default withStyles(styles)(RoomState);
