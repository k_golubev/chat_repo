import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

import withStyles from '@material-ui/core/styles/withStyles';

import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import IconButton from '@material-ui/core/IconButton';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';

import LanguageIcon from '@material-ui/icons/Language';

import { getLanguages } from 'Actions/language';
import { getAllLanguages } from 'Selectors/language';

import styles from 'Styles/app/header/languages';

function Languages (props) {
  const { classes } = props;

  const { i18n, t } = useTranslation();

  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const languages = useSelector((state) => getAllLanguages(state));

  useEffect(() => {
    if (!languages.length) {
      document.title = `${t('index_page.title')} - Chtface`;

      dispatch(getLanguages());
    }
  }, []);

  const toggleOpenList = () => {
    setOpen(!open);
  };

  const changeLanguageCallback = (lng) => {
    i18n.changeLanguage(lng, () => {
      document.title = `${t('index_page.title')} - Chtface`;
    });
  };

  const handleChangeLanguage = lng => {
    toggleOpenList();

    if (i18n.languages.includes(lng)) {
      changeLanguageCallback(lng);

      return;
    }

    i18n.loadLanguages(lng, () => {
      changeLanguageCallback(lng);
    });
  };

  if (!i18n || !languages.length) return null;

  return (
    <>
      <Tooltip placement="bottom" title={t('actions_bar.change_language')}>
        <IconButton onClick={toggleOpenList} buttonRef={(node) => setAnchorEl(node)}>
          <LanguageIcon className={classes.icon}/>
        </IconButton>
      </Tooltip>

      <Typography classes={{ root: classes.lang }}>
        {i18n.language}
      </Typography>

      <Popper
        open={open}
        anchorEl={anchorEl}
        transition
        disablePortal
        className={classes.popper}
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
          >
            <Paper>
              <ClickAwayListener onClickAway={toggleOpenList}>
                <MenuList>
                  {languages.map(item => (
                    <MenuItem
                      key={item._id}
                      onClick={() => handleChangeLanguage(item.key)}
                      classes={{ root: classes.rootMenuItem, selected: classes.selectedMenuItem }}
                      selected={item.key === i18n.language}
                    >
                      {item.key}
                    </MenuItem>
                  ))}
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </>
  );
};

Languages.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Languages);
