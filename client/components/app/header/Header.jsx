import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import withStyles from '@material-ui/core/styles/withStyles';
import { Link } from 'react-router-dom';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';

import ActionsBar from 'Components/app/header/ActionsBar';
import MainMenu from 'Components/app/MainMenu';

import MenuIcon from '@material-ui/icons/Menu';

import { getIsSU } from 'Selectors/current-user';

import styles from 'Styles/app/header/header';

function Header (props) {
  const { classes } = props;
  const [openMenu, setOpenMenu] = useState(false);

  const isSU = useSelector((state) => getIsSU(state));

  const handleToggleMenu = () => {
    setOpenMenu(!openMenu);
  };

  return (
    <>
      <AppBar position="static">
        <Toolbar>
          {isSU && (
            <IconButton
              edge="start"
              color="inherit"
              aria-label="menu"
              onClick={handleToggleMenu}
            >
              <MenuIcon />
            </IconButton>
          )}
          <Link to="/">
            <img src="/images/logos/logo_white.png" alt="Chatface" className={classes.logo} />
          </Link>
          <div className={classes.grow} />
          <ActionsBar />
        </Toolbar>
      </AppBar>

      {isSU && <MainMenu open={openMenu} onClose={handleToggleMenu}/>}
    </>
  );
}

Header.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Header);
