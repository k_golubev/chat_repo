import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';

import withStyles from '@material-ui/core/styles/withStyles';

import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

import AddIcon from '@material-ui/icons/ControlPoint';

import { setIsRequestForm } from 'Actions/app';

import styles from 'Styles/app/header/actions-bar';

function AddRequest (props) {
  const { classes } = props;

  const dispatch = useDispatch();
  const { t } = useTranslation();

  return (
    <Tooltip placement="bottom" title={t('actions_bar.create_request')}>
      <IconButton onClick={() => dispatch(setIsRequestForm(true))}>
        <AddIcon className={classes.icon} />
      </IconButton>
    </Tooltip>
  );
};

AddRequest.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AddRequest);
