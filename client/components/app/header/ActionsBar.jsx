import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

import withStyles from '@material-ui/core/styles/withStyles';

import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

import ExitIcon from '@material-ui/icons/ExitToApp';
import SettingsIcon from '@material-ui/icons/SettingsApplicationsRounded';

import Languages from 'Components/app/header/Languages';
import ParticipantStateButton from 'Components/app/header/ParticipantStateButton';
import AddRequest from 'Components/app/header/AddRequest';

import { deleteRequest, setActiveRequest } from 'Actions/requests';
import { setIsExit } from 'Actions/app';
import { setActiveConversation } from 'Actions/conversations';

import { getCurrentUser } from 'Selectors/current-user';
import { getActiveRequest } from 'Selectors/requests';

import styles from 'Styles/app/header/actions-bar';

import socket from 'Modules/socket';

function ActionsBar (props) {
  const { classes } = props;

  const dispatch = useDispatch();
  const { t } = useTranslation();

  const user = useSelector((state) => getCurrentUser(state));
  const request = useSelector((state) => getActiveRequest(state));

  const onChangeSettings = () => {
    dispatch(deleteRequest(request._id));

    socket.emit('leave-conversation', user._id);

    dispatch(setActiveRequest(null));
    dispatch(setActiveConversation(null));
  };

  return (
    <>
      {user.isSU && <AddRequest />}

      {request && (
        <div>
          {request.isProcessing ? (
            <>
              <ParticipantStateButton />

              <Tooltip placement="bottom" title={t('actions_bar.end_chat')}>
                <IconButton onClick={() => dispatch(setIsExit(true))}>
                  <ExitIcon className={classes.icon}/>
                </IconButton>
              </Tooltip>
            </>
          ) : (
            <>
              <Tooltip placement="bottom" title={t('actions_bar.change_settings')}>
                <IconButton onClick={onChangeSettings}>
                  <SettingsIcon className={classes.icon} />
                </IconButton>
              </Tooltip>
            </>
          )}
        </div>
      )}

      <Suspense fallback={<div />}>
        <Languages />
      </Suspense>
    </>
  );
};

ActionsBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ActionsBar);
