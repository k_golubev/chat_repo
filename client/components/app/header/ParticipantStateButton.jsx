import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import withStyles from '@material-ui/core/styles/withStyles';

import ParticipantStateModal from 'Components/app/modals/ParticipantStateModal';

import IconButton from '@material-ui/core/IconButton';

import StateIcon from '@material-ui/icons/Info';

import { getParticipantState } from 'Selectors/conversations';

import styles from 'Styles/app/header/participant-state-button';

function ParticipantStateButton (props) {
  const { classes } = props;
  const [openState, setOpenState] = useState(false);

  const participantState = useSelector((state) => getParticipantState(state));

  const onToggleState = () => {
    setOpenState(!openState);
  };

  if (!participantState) return null;

  return (
    <>
      <IconButton onClick={onToggleState}>
        <StateIcon className={classes.icon}/>
      </IconButton>

      {openState && <ParticipantStateModal participantState={participantState} onClose={onToggleState} />}
    </>
  );
};

ParticipantStateButton.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ParticipantStateButton);
