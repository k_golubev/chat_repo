import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { useSelector } from 'react-redux';

import withStyles from '@material-ui/core/styles/withStyles';

import Container from '@material-ui/core/Container';
import StartScreen from 'Components/app/StartScreen';
import Chat from 'Components/app/Chat';

import ExitChat from 'Components/app/modals/ExitChat';
import AcceptChat from 'Components/app/modals/AcceptChat';

import { getIsExit, getIsAccept, getIsSettingsOpen } from 'Selectors/app';
import { getActiveRequest } from 'Selectors/requests';

import styles from 'Styles/app/main';

function MainPage (props) {
  const { classes, participantLeavedRoom, typing } = props;

  const request = useSelector((state) => getActiveRequest(state));
  const isSettingsOpen = useSelector((state) => getIsSettingsOpen(state));
  const isExit = useSelector((state) => getIsExit(state));
  const isAccept = useSelector((state) => getIsAccept(state));

  const containerClasses = [classes.root];

  if (!request) {
    containerClasses.push(classes.startScreenContainer);

    if (isSettingsOpen) containerClasses.push(classes.flexStart);
  }

  return (
    <>
      <Container maxWidth="md" classes={{ root: classnames(containerClasses) }}>
        {!request ? (
          <StartScreen />
        ) : (
          <Chat typing={typing} />
        )}
      </Container>

      {isExit && <ExitChat participantLeavedRoom={participantLeavedRoom} />}
      {isAccept && <AcceptChat />}
    </>
  );
};

MainPage.propTypes = {
  classes: PropTypes.object.isRequired,
  participantLeavedRoom: PropTypes.bool.isRequired,
  typing: PropTypes.bool.isRequired
};

export default withStyles(styles)(MainPage);
