import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

import withStyles from '@material-ui/core/styles/withStyles';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import styles from 'Styles/app/modals/participant-state-modal';

function ParticipantStateModal (props) {
  const { classes, onClose, participantState } = props;

  const { t } = useTranslation();

  return (
    <Dialog
      open
      disableBackdropClick
      disableEscapeKeyDown
      classes={{ paper: classes.paper }}
    >
      <DialogTitle>
        {t('participantstate')}
      </DialogTitle>
      <DialogContent classes={{ root: classes.content }}>
        {participantState.state}
      </DialogContent>

      <DialogActions>
        <Button color="primary" onClick={onClose}>
          {t('close')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

ParticipantStateModal.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  participantState: PropTypes.object.isRequired
};

export default withStyles(styles)(ParticipantStateModal);
