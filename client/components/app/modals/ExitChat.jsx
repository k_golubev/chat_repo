import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

import withStyles from '@material-ui/core/styles/withStyles';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import { setActiveRequest, deleteRequest, updateRequest as updateRequestProps } from 'Actions/requests';
import { deleteConversation } from 'Actions/conversations';
import { setIsExit } from 'Actions/app';

import { getCurrentUser } from 'Selectors/current-user';
import { getActiveRequest } from 'Selectors/requests';
import { getActiveConversation } from 'Selectors/conversations';

import { updateRequest } from 'Rest/requests';

import styles from 'Styles/app/modals/exit-chat';

import socket from 'Modules/socket';

function ExitChat (props) {
  const { classes, participantLeavedRoom } = props;

  const { t } = useTranslation();
  const dispatch = useDispatch();

  const conversation = useSelector((state) => getActiveConversation(state));
  const user = useSelector((state) => getCurrentUser(state));
  const request = useSelector((state) => getActiveRequest(state));

  const onChangeSettings = () => {
    if (request) dispatch(deleteRequest(request._id));

    socket.emit('leave-conversation', user._id);

    dispatch(setActiveRequest(null));
    if (conversation) dispatch(deleteConversation(conversation._id));
    dispatch(setIsExit(false));
  };

  const onChangeParticipant = () => {
    if (request) {
      request.status = 'pending';

      updateRequest(request).then((request) => {
        socket.emit('change-participant', user._id);

        dispatch(updateRequestProps(request));
      });
    }

    dispatch(setIsExit(false));
    if (conversation) dispatch(deleteConversation(conversation._id));
  };

  const onClose = () => {
    if (!participantLeavedRoom) dispatch(setIsExit(false));
  };

  const title = participantLeavedRoom ? t('exitform.user_leaved_room') : t('exitform.end_chat');

  return (
    <Dialog
      open
      disableBackdropClick={participantLeavedRoom}
      disableEscapeKeyDown={participantLeavedRoom}
      classes={{ paper: classes.paper }}
      onClose={onClose}
    >
      <DialogTitle classes={{ root: classes.title }}>
        {title}
      </DialogTitle>
      <DialogContent classes={{ root: classnames([classes.content, participantLeavedRoom ? classes.withoutActions : '']) }}>
        <Button color="primary" variant="contained" onClick={onChangeSettings} >
          {t('exitform.change_settings')}
        </Button>
        <Button color="primary" variant="contained" onClick={onChangeParticipant} >
          {t('exitform.find_new_interlocutor')}
        </Button>
      </DialogContent>

      {!participantLeavedRoom && (
        <DialogActions classes={{ root: classes.actions }}>
          <Button color="primary" onClick={onClose}>
            {t('cancel')}
          </Button>
        </DialogActions>
      )}
    </Dialog>
  );
};

ExitChat.propTypes = {
  classes: PropTypes.object.isRequired,
  participantLeavedRoom: PropTypes.bool.isRequired
};

export default withStyles(styles)(ExitChat);
