import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

import withStyles from '@material-ui/core/styles/withStyles';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import { setActiveRequest, updateRequest as updateRequestProps } from 'Actions/requests';
import { setActiveConversation } from 'Actions/conversations';
import { setIsAccept } from 'Actions/app';

import { getCurrentUser } from 'Selectors/current-user';
import { getActiveConversation, getParticipantState } from 'Selectors/conversations';
import { getActiveRequest } from 'Selectors/requests';

import { getRequest, updateRequest } from 'Rest/requests';

import styles from 'Styles/app/modals/accept-chat';

import socket from 'Modules/socket';

function AcceptChat (props) {
  const { classes } = props;

  const { t } = useTranslation();
  const dispatch = useDispatch();

  const user = useSelector((state) => getCurrentUser(state));
  const conversation = useSelector((state) => getActiveConversation(state));
  const participantState = useSelector((state) => getParticipantState(state));
  const request = useSelector((state) => getActiveRequest(state));

  const onSearchingFurther = () => {
    request.status = 'pending';
    request.conversation = conversation;

    updateRequest(request).then((request) => {
      socket.emit('change-participant', user._id);

      if (request) dispatch(updateRequestProps(request));
      dispatch(setActiveConversation(null));
      dispatch(setIsAccept(false));
    });
  };

  const onAcceptChat = () => {
    getRequest(request._id).then((request) => {
      dispatch(setActiveRequest(request));
      dispatch(setIsAccept(false));
    });
  };

  if (!participantState) return null;

  return (
    <Dialog
      open={!!conversation}
      disableBackdropClick
      disableEscapeKeyDown
      classes={{ paper: classes.paper }}
    >
      <DialogTitle>
        {t('acceptchat.title')}
      </DialogTitle>
      <DialogContent classes={{ root: classes.content }}>
        {participantState.state}
      </DialogContent>

      <DialogActions>
        <Button color="primary" onClick={onSearchingFurther}>
          {t('acceptchat.searching_further')}
        </Button>
        <Button color="primary" variant="contained" onClick={onAcceptChat}>
          {t('acceptchat.start_chat')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

AcceptChat.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AcceptChat);
