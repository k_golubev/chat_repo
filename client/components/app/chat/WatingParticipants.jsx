import React from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';

import CircularProgress from '@material-ui/core/CircularProgress';

import styles from 'Styles/app/chat/waiting-participants';

function WaitingParticipants (props) {
  const { classes, size } = props;

  return (<div className={classes.root}><CircularProgress size={size} /></div>);
};

WaitingParticipants.propTypes = {
  classes: PropTypes.object.isRequired,
  size: PropTypes.number
};

WaitingParticipants.defaultProps = {
  size: 80
};

export default withStyles(styles)(WaitingParticipants);
