import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import withStyles from '@material-ui/core/styles/withStyles';

import Fab from '@material-ui/core/Fab';
import TextField from '@material-ui/core/TextField';

import SendIcon from '@material-ui/icons/Send';

import { getCurrentUser } from 'Selectors/current-user';
import { getActiveConversation } from 'Selectors/conversations';

import styles from 'Styles/app/chat/input-field';

import socket from 'Modules/socket';

let typingTimer = null;

function InputField (props) {
  const { classes, defaultValue, onSendMessage } = props;
  const { t } = useTranslation();

  const [value, setValue] = useState(defaultValue);

  const user = useSelector((state) => getCurrentUser(state));
  const conversation = useSelector((state) => getActiveConversation(state));

  const onSubmit = () => {
    setValue(defaultValue);

    onSendMessage(value);
  };

  const onKeyUp = (e) => {
    if (e.keyCode === 13) onSubmit();
  };

  const typing = (typing) => {
    socket.emit('typing', { typing, conversation: conversation._id, user: user._id });
  };

  const onChange = (e) => {
    setValue(e.target.value);

    typing(true);

    if (typingTimer) clearInterval(typingTimer);

    typingTimer = setTimeout(() => {
      typing(false);
    }, 1000);
  };

  return (
    <TextField
      fullWidth
      placeholder={t('chat.type_message')}
      value={value}
      InputProps={{
        classes: { input: classes.inputRoot },
        disableUnderline: true,
        endAdornment: (
          <Fab color="primary" size="medium" onClick={onSubmit}>
            <SendIcon />
          </Fab>
        )
      }}
      className={classes.root}
      onChange={onChange}
      onKeyUp={onKeyUp}
    />
  );
};

InputField.propTypes = {
  classes: PropTypes.object.isRequired,
  defaultValue: PropTypes.string,
  onSendMessage: PropTypes.func.isRequired
};

InputField.defaultProps = {
  defaultValue: ''
};

export default withStyles(styles)(InputField);
