import React from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'react-i18next';

import moment from 'moment';
import classnames from 'classnames';

import withStyles from '@material-ui/core/styles/withStyles';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';

import styles from 'Styles/app/chat/message';

function Message (props) {
  const { classes, isYour, message } = props;
  const { t } = useTranslation();

  const bodyClasses = [classes.body];
  const listItemClasses = [classes.item];

  if (isYour) {
    bodyClasses.push(classes.yourBody);
    listItemClasses.push(classes.yourItem);
  }

  return (
    <ListItem disableGutters>
      <ListItemText
        className={classnames(listItemClasses)}
        primary={(
          <Typography
            component="span"
            dangerouslySetInnerHTML={{ __html: message.body }}
            className={classnames(bodyClasses)}
          />
        )}
        secondary={(
          <Typography className={classes.date}>
            {moment(message.createdAt).format(t('timeFormat'))}
          </Typography>
        )}
      />
    </ListItem>
  );
};

Message.propTypes = {
  classes: PropTypes.object.isRequired,
  message: PropTypes.object.isRequired,
  isYour: PropTypes.bool
};

Message.defaultProps = {
  isYour: true
};

export default withStyles(styles)(Message);
