import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import moment from 'moment';

import { Scrollbars } from 'react-custom-scrollbars';

import withStyles from '@material-ui/core/styles/withStyles';

import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';

import Message from 'Components/app/chat/Message';

import { getCurrentUser } from 'Selectors/current-user';
import { getMessages } from 'Selectors/conversations';

import styles from 'Styles/app/chat/histroy';

const getProcessedHistory = history => {
  return history.reduce((obj, message, index) => {
    const currentMessageTime = moment(message.createdAt);

    if (index > 0) {
      const lastMessage = obj[obj.length - 1];
      const timeDiff = currentMessageTime.diff(lastMessage.createdAt, 'minutes');

      if (lastMessage.from === message.from && timeDiff < 10) {
        lastMessage.body += `<br />${message.body}`;
        lastMessage.createdAt = message.time;
      } else {
        obj.push(message);
      }
    } else {
      obj.push(message);
    }

    return obj;
  }, []);
};

function History (props) {
  const { classes } = props;

  const user = useSelector((state) => getCurrentUser(state));
  const messages = useSelector((state) => getMessages(state));

  const history = useMemo(() => getProcessedHistory(messages), [messages]);

  return (
    <Grid container item classes={{ container: classes.root }}>
      <Scrollbars>
        <List disablePadding className={classes.list}>
          {history.map((message, index) => {
            const isYour = message.from === user._id;

            return <Message message={message} key={`message-${index}`} isYour={isYour} />;
          })}
        </List>
      </Scrollbars>
    </Grid>
  );
};

History.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(History);
