import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

import withStyles from '@material-ui/core/styles/withStyles';

import Typography from '@material-ui/core/Typography';

import styles from 'Styles/app/chat/typing';

function Typing (props) {
  const { classes } = props;

  const { t } = useTranslation();

  return (
    <Typography classes={{ root: classes.root }} component="span">
      {t('typing')}
    </Typography>
  );
};

Typing.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Typing);
