export default theme => ({
  root: {
    marginTop: 64,
    marginBottom: 64,
    [theme.breakpoints.down('sm')]: {
      marginTop: 32,
      marginBottom: 32
    }
  },

  fab: {
    margin: 8
  }
});
