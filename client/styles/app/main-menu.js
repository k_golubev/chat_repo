export default () => ({
  paperAnchorLeft: {
    minWidth: 250
  },

  logo: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 16,
    marginBottom: 16,

    '& > img': {
      width: 150
    }
  }
});
