export default () => ({
  actions: {
    paddingRight: 24,
    paddingLeft: 24,
    paddingTop: 16,
    paddingBottom: 24
  }
});
