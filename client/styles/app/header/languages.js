import colors from 'Styles/colors';

export default () => ({
  popper: {
    zIndex: 1
  },

  icon: {
    color: colors.white
  },

  lang: {
    textTransform: 'capitalize',
    minWidth: 30,
    textAlign: 'left',
    fontWeight: 600,
    marginLeft: 8
  },

  rootMenuItem: {
    textTransform: 'capitalize'
  }
});
