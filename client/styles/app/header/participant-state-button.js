import colors from 'Styles/colors';

export default () => ({
  icon: {
    color: colors.white
  }
});
