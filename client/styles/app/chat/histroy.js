export default () => ({
  root: {
    height: 'calc(100% - 80px)',
    paddingTop: 16,
    paddingBottom: 16
  },

  list: {
    width: '100%',
    paddingRight: 16,
    paddingLeft: 16
  }
});
