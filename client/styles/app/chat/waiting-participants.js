import colors from 'Styles/colors';
import hexToRGBA from 'Modules/hex-to-rgb';

export default () => ({
  root: {
    position: 'absolute',
    top: 0,
    width: '100%',
    height: '100%',
    backgroundColor: hexToRGBA(colors.lightGrayPalette[200], 0.6),
    borderRadius: 4,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
