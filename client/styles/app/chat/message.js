import colors from 'Styles/colors';

export default (theme) => ({
  item: {
    textAlign: 'left'
  },

  yourItem: {
    textAlign: 'right'
  },

  body: {
    borderRadius: '10px 10px  10px 0px',
    fontSize: 14,
    backgroundColor: colors.lightGrayPalette[100],
    padding: 10,
    display: 'inline-flex'
  },

  yourBody: {
    backgroundColor: theme.palette.primary.main,
    color: colors.white,
    borderRadius: '10px 10px  0px 10px'
  },

  date: {
    fontSize: 12,
    color: `${colors.lightGrayPalette[500]}`,
    marginTop: 4
  }
});
