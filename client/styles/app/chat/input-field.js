import colors from 'Styles/colors';

export default () => ({
  root: {
    height: 80,
    paddingTop: 16,
    paddingRight: 16,
    paddingLeft: 16,
    borderTopStyle: 'solid',
    borderTopWidth: 1,
    borderTopColor: colors.lightGrayPalette[200],

    '& > div:first-child': {
      justifyContent: 'space-evenly'
    }
  },

  inputRoot: {
    width: 'calc(100% - 56px)'
  }
});
