export default () => ({
  root: {
    position: 'absolute',
    bottom: 88,
    left: 16,
    fontStyle: 'italic',
    fontSize: 14,
    display: 'block',
    width: '100%',
    textAlign: 'center'
  }
});
