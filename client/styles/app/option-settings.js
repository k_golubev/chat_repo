export default theme => ({
  root: {
    marginTop: 48
  },

  optionsRoot: {
    marginTop: 16,
    justifyContent: 'center',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      alignItems: 'center'
    }
  },

  icon: {
    marginLeft: 8,
    fontSize: 18
  },

  toggle: {
    display: 'flex',
    cursor: 'pointer',
    margin: 'auto',
    fontSize: 18,
    alignItems: 'center'
  }
});
