export default () => ({
  root: {
    height: 'calc(100vh - 64px)',
    padding: 16
  },

  startScreenContainer: {
    display: 'flex',
    alignItems: 'center'
  },

  flexStart: {
    alignItems: 'flex-start'
  }
});
