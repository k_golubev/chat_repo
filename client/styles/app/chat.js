export default () => ({
  root: {
    height: '100%'
  },

  paper: {
    height: '100%',
    position: 'relative'
  },

  wrapper: {
    height: '100%'
  }
});
