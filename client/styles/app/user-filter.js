export default theme => ({
  root: {
    maxWidth: 250,
    display: 'flex',
    marginRight: 24,
    marginLeft: 24,
    marginTop: 0,
    marginBottom: 0,
    [theme.breakpoints.down('sm')]: {
      maxWidth: '100%',
      marginRight: 0,
      marginLeft: 0
    }
  },

  item: {
    display: 'flex',
    flexDirection: 'column'
  },

  formControl: {
    marginBottom: 16
  },

  legend: {
    fontSize: 13
  },

  label: {
    fontSize: 16,
    width: '100%',
    marginBottom: 32,
    display: 'block'
  }
});
