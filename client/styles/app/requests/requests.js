export default () => ({
  root: {
    height: 'calc(100vh - 64px)',
    padding: 16
  },

  container: {
    height: 'auto'
  },

  filters: {
    paddingTop: 8,
    paddingBottom: 8,
    alignItems: 'baseline'
  },

  search: {
    paddingTop: 16,
    paddingBottom: 16
  },

  virtualizedList: {
    '&:focus': {
      outline: 0
    },

    height: 600
  },

  listItem: {},

  request: {
    paddingBottom: 8,
    paddingTop: 8,
    display: 'block'
  },

  bold: {
    fontWeight: 600
  },

  metaData: {
    display: 'flex',
    alignItems: 'center'
  },

  metaDataItem: {
    fontSize: 15,
    display: 'flex',
    alignItems: 'center',
    marginRight: 16,

    '& > svg': {
      fontSize: 15,
      marginRight: 4
    },

    '&:last-child': {
      marginRight: 0
    }
  },

  link: {
    cursor: 'pointer'
  },

  grow: {
    flexGrow: 1
  },

  fileInputHidden: {
    display: 'none'
  },

  uploadCsv: {
    marginRight: 16
  }
});
