export default () => ({
  root: {
    margin: 8,
    minWidth: 120,

    '&:first-child': {
      marginLeft: 0
    }
  }
});
