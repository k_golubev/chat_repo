export default () => ({
  paper: {
    minWidth: 320
  },

  content: {
    marginBottom: 16
  }
});
