export default () => ({
  paper: {
    minWidth: 320
  },

  title: {
    textAlign: 'center'
  },

  content: {
    display: 'flex',
    flexDirection: 'column',

    '& button': {
      marginBottom: 16
    },

    '& button:last-child': {
      marginBottom: 0
    }
  },

  actions: {
    justifyContent: 'center'
  },

  withoutActions: {
    paddingBottom: 16
  }
});
