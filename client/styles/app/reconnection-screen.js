import colors from 'Styles/colors';
import hexToRGBA from 'Modules/hex-to-rgb';

export default (theme) => ({
  container: {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100vw',
    height: '100vh',
    color: colors.white,
    backgroundColor: hexToRGBA(colors.black, 0.6),
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: '50px',
    zIndex: 1000000,
    [theme.breakpoints.down('md')]: {
      fontSize: '40px'
    },
    [theme.breakpoints.down('sm')]: {
      fontSize: '30px'
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: '20px'
    }
  }
});
