import { createMuiTheme } from '@material-ui/core/styles';
import colors from 'Styles/colors';

export const defaultTheme = {
  breakpoints: {
    values: {
      xs: 320,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1920
    }
  },
  palette: {
    common: {
      black: colors.black,
      white: colors.white
    }
  }
};

export default createMuiTheme(defaultTheme);
