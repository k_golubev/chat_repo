export default {
  white: '#ffffff',
  black: '#000000',

  // palettes
  lightGrayPalette: {
    50: '#F7F9FA',
    100: '#e7ebec',
    200: '#d8dee0',
    300: '#c8d0d4',
    400: '#bcc6ca',
    500: '#b0bcc1',
    600: '#a9b6bb',
    700: '#a0adb3',
    800: '#97a5ab',
    900: '#87979e',
    A100: '#ffffff',
    A200: '#fdfeff',
    A400: '#c9eeff',
    A700: '#b0e6ff',
    contrastDefaultColor: 'dark',
    main: '#b0bcc1',
    contrastText: '#000000'
  }
};
