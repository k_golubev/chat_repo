import {
  SET_LANGUAGES
} from 'Types/language';

const initialState = {
  list: []
};

const actions = {
  [SET_LANGUAGES]: 'list'
};

export default (state = initialState, action) => {
  if (action.type in actions) {
    return { ...state, [actions[action.type]]: action.payload };
  }

  return state;
};
