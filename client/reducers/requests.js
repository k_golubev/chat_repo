import {
  SET_REQUESTS,
  SET_ACTIVE_REQUEST
} from 'Types/requests';

const initialState = {
  list: [],
  active: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_REQUESTS:
      return { ...state, list: action.payload };
    case SET_ACTIVE_REQUEST:
      if (action.payload && !state.list.some((request) => request._id === action.payload._id)) {
        state.list.push(action.payload);
      }

      return { ...state, active: action.payload };
    default:
      return state;
  }
};
