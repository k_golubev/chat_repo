import {
  SET_ACTIVE_CONVERSATION,
  SET_CONVERSATIONS
} from 'Types/conversations';

const initialState = {
  list: [],
  active: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_ACTIVE_CONVERSATION:
      return { ...state, active: action.payload };
    case SET_CONVERSATIONS:
      return { ...state, list: action.payload };
    default:
      return state;
  }
};
