import { combineReducers } from 'redux';

import app from '../app';
import currentUser from '../current-user';
import conversations from '../conversations';
import language from '../language';
import requests from '../requests';

export default combineReducers({
  app,
  currentUser,
  conversations,
  language,
  requests
});
