import {
  SET_USER,
  SET_ONLINE
} from 'Types/current-user';

const initialState = {
  user: null,
  isOnline: true
};

const actions = {
  [SET_USER]: 'user',
  [SET_ONLINE]: 'isOnline'
};

export default (state = initialState, action) => {
  if (action.type in actions) {
    return { ...state, [actions[action.type]]: action.payload };
  }

  return state;
};
