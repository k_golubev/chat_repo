import {
  SET_IS_EXIT,
  SET_IS_ACCEPT,
  SET_SETTINGS_OPEN,
  SET_IS_REQUEST_FORM
} from 'Types/app';

const initialState = {
  isExit: false,
  isAccept: false,
  isSettingsOpen: false,
  isRequestForm: false
};

const actions = {
  [SET_IS_EXIT]: 'isExit',
  [SET_IS_ACCEPT]: 'isAccept',
  [SET_SETTINGS_OPEN]: 'isSettingsOpen',
  [SET_IS_REQUEST_FORM]: 'isRequestForm'
};

export default (state = initialState, action) => {
  if (action.type in actions) {
    return { ...state, [actions[action.type]]: action.payload };
  }

  return state;
};
