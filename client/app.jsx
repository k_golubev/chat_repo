import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';

import App from 'Containers/App';
import Theme from 'Containers/Theme';
import reducers from 'Reducers/containers/app';

import './modules/i18n';

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__;

const store = devTools ? createStore(
  reducers,
  compose(applyMiddleware(thunk), devTools())
) : createStore(reducers, applyMiddleware(thunk));

const Main = () => (
  <Provider store={store}>
    <Theme>
      <App />
    </Theme>
  </Provider>
);

ReactDOM.render(<Main />, document.getElementById('root'));
