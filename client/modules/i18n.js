import i18n from 'i18next';
import XHR from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';

const allLanguages = ['ru', 'en', 'fr', 'de', 'es', 'zh', 'pt'];

i18n
  .use(XHR)
  .use(LanguageDetector)
  .use(initReactI18next) // bind react-i18next to the instance
  .init({
    lng: 'ru',
    fallbackLng: 'ru',
    whitelist: allLanguages,
    //  debug: true,

    interpolation: {
      escapeValue: false // react already safes from xss
    },
    backend: {
      // for all available options read the backend's repository readme file
      loadPath: '/locales/{{lng}}'
    }
  });

export default i18n;
