import ReactGA from 'react-ga';

ReactGA.initialize('UA-147545375-1');

const fakeGAForDev = {
  pageview () {},
  modalview () {},
  event () {}
};

export default process.env.NODE_ENV === 'production' ? ReactGA : fakeGAForDev;
