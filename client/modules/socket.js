import io from 'socket.io-client';

const socket = io(process.env.ORIGIN, {
  reconnectionDelay: 1000
});

socket.on('logout', () => {
  window.location.reload();
});

socket.on('connection', (socket) => {
  console.log(socket);
});

export default socket;
