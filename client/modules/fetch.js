import 'whatwg-fetch';
import Promise from 'promise-polyfill';

if (!window.Promise) window.Promise = Promise;

class Fetch {
  static dispatch (...args) {
    return Fetch.store && Fetch.store.dispatch(...args);
  }

  static defaultOptions () {
    return {
      cache: 'default',
      credentials: 'same-origin'
    };
  }

  static handler (url, options, resolve, reject) {
    const fetchPromise = fetch(url, { ...Fetch.defaultOptions(), ...options });
    let responseStatus;
    let _response;

    return fetchPromise.then((response) => {
      const json = response.json();
      const { status } = response;

      responseStatus = status;

      if (responseStatus >= 200 && responseStatus < 300) return json;

      return json.then((res) => {
        _response = res;

        return reject({ message: res.message, responseStatus, response: _response });
      });
    }).then(resolve).catch((ex) => {
      return reject({ message: ex.toString(), responseStatus, response: _response });
    });
  }

  static get (url, options = {}) {
    return new Promise((resolve, reject) => {
      Fetch.handler(url, options, resolve, reject);
    });
  }

  static post (url, data, options = {}) {
    return new Promise((resolve, reject) => {
      Fetch.handler(url, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8'
        },
        body: JSON.stringify(data),
        ...options
      }, resolve, reject);
    });
  }

  static put (url, data, options = {}) {
    return new Promise((resolve, reject) => {
      Fetch.handler(url, {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8'
        },
        body: JSON.stringify(data),
        ...options
      }, resolve, reject);
    });
  }

  static delete (url, data, options = {}) {
    return new Promise((resolve, reject) => {
      Fetch.handler(url, {
        method: 'DELETE',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json; charset=utf-8'
        },
        body: JSON.stringify(data),
        ...options
      }, resolve, reject);
    });
  }
}

export default Fetch;
