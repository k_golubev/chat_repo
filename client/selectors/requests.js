export const getActiveRequest = state => state.requests.active;
export const getRequests = state => state.requests.list;
