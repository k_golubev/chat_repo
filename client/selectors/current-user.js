import { createSelector } from 'reselect';

export const getCurrentUser = state => state.currentUser.user;
export const getOnline = state => state.currentUser.isOnline;

export const getIsSU = createSelector(
  [getCurrentUser],
  (user) => user ? user.isSU : false
);
