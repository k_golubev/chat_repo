import { createSelector } from 'reselect';

import { getCurrentUser } from 'Selectors/current-user';

export const getActiveConversation = state => state.conversations.active;
export const getConversations = state => state.conversations.list;

export const getParticipantState = createSelector(
  [getCurrentUser, getActiveConversation],
  (user, conversation) => conversation ? conversation.states.find((state) => state.user !== user._id && state.state.length) : null
);

export const getMessages = createSelector(
  [getActiveConversation],
  (conversation) => conversation ? conversation.messages : []
);
