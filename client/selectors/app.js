export const getIsExit = state => state.app.isExit;
export const getIsAccept = state => state.app.isAccept;
export const getIsSettingsOpen = state => state.app.isSettingsOpen;
export const getIsRequestForm = state => state.app.isRequestForm;
