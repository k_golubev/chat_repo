import fetch from 'Modules/fetch';

export const getSelfUser = () => fetch.get('/users/current');
