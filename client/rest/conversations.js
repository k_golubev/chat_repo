import fetch from 'Modules/fetch';

export const getConversations = () => fetch.get('/app/conversations');
