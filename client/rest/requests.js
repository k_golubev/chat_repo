import fetch from 'Modules/fetch';

export const getRequests = () => fetch.get('/app/requests');

export const getRequest = (requestId) => fetch.get(`/app/requests/${requestId}`);

export const createRequest = (settings) => fetch.post('/app/requests', { settings });

export const updateRequest = (request, options) => fetch.put(`/app/requests/${request.id}`, request, options);

export const deleteRequest = (requestId) => fetch.delete(`/app/requests/${requestId}`);

export const parseRequestsCSV = (body, options) => fetch.post('/app/requests/import', body, options);

export const removeAllRequest = () => fetch.post('/app/requests/clear');
