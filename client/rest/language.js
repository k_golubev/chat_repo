import fetch from 'Modules/fetch';

export const getLanguages = () => fetch.get('/locales');
