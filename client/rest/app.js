import fetch from 'Modules/fetch';

export const onStart = settings => fetch.post('/app/start', { settings });

export const onSendMessage = message => fetch.post('/app/message', message);
