import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import orderBy from 'lodash/orderBy';

import { BrowserRouter, Route } from 'react-router-dom';

import Header from 'Components/app/header/Header';

import LoginPage from 'Components/app/LoginPage';
import MainPage from 'Components/app/MainPage';

// Requests
import Requests from 'Components/app/Requests';
import RequestFrom from 'Components/app/requests/RequestForm';

import { setIsExit, setIsAccept } from 'Actions/app';
import {
  setActiveConversation,
  updateConversation,
  setConversations,
  deleteConversation
} from 'Actions/conversations';
import { setUser } from 'Actions/current-user';
import { setActiveRequest, setRequests } from 'Actions/requests';

import { getIsExit } from 'Selectors/app';
import { getCurrentUser } from 'Selectors/current-user';
import { getActiveConversation, getConversations as getConversationsProps } from 'Selectors/conversations';
import { getRequests as getRequestsProps } from 'Selectors/requests';

import { getSelfUser } from 'Rest/current-user';
import { getRequests } from 'Rest/requests';
import { getConversations } from 'Rest/conversations';

import socket from 'Modules/socket';
import i18n from 'Modules/i18n';

class App extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      participantLeavedRoom: false,
      languageLoaded: false,
      typing: false,
      connected: false
    };

    this.initState = this.initState.bind(this);
    this.getSelfUser = this.getSelfUser.bind(this);

    this.handleLoad = this.handleLoad.bind(this);

    i18n.on('loaded', () => {
      this.setState({ languageLoaded: true });
    });
  }

  componentDidMount () {
    socket.on('connect', () => {
      this.getSelfUser();
    });

    socket.on('connected', (response) => {
      const { dispatch } = this.props;

      if (!response.user) {
        this.getSelfUser();

        return;
      }

      dispatch(setUser(response.user));

      this.handleLoad();
    });

    socket.on('update-conversation', (conversation) => {
      const { dispatch } = this.props;

      dispatch(updateConversation(conversation));
    });

    socket.on('start-conversation', (conversation) => {
      const { dispatch, user } = this.props;

      const participantState = user ? conversation.states.find((state) => state.user !== user._id) : null;

      if (participantState && participantState.state.length) {
        dispatch(setIsAccept(true));
      }

      getSelfUser().then((response) => {
        if (response) {
          dispatch(setUser(user));
          dispatch(setActiveConversation(conversation));

          socket.emit('join', { userId: response._id });
        }
      });
    });

    socket.on('participan-leave-conversation', (response) => {
      const {
        dispatch,
        conversation,
        conversations,
        requests,
        user
      } = this.props;

      const { userId, conversationId } = response;

      if (userId === user._id) return;

      dispatch(setIsAccept(false));

      if (conversation && conversation._id === conversationId && userId !== user._id) {
        this.setState({ participantLeavedRoom: true }, () => dispatch(setIsExit(true)));
      }

      const conversationInProps = conversations.find((item) => item._id === conversationId);

      if (conversationInProps) {
        conversationInProps.requests.forEach((request) => {
          const requestFromProps = requests.find((item) => item._id === request);

          if (requestFromProps) {
            if (requestFromProps.user !== user._id) {
              const nextRequests = requests.filter((item) => item._id !== requestFromProps._id);

              dispatch(setRequests(nextRequests));
            }
          }
        });

        dispatch(deleteConversation(conversationId));
      }
    });

    socket.on('typing', (data) => {
      const { typing } = this.state;

      if (data.typing !== typing) this.setState({ typing: data.typing });
    });
  }

  componentDidUpdate (prevProps) {
    const { isExit } = this.props;

    if (prevProps.isExit && !isExit) this.initState();
  }

  getSelfUser () {
    const { dispatch } = this.props;

    getSelfUser().then((user) => {
      if (user) dispatch(setUser(user));

      socket.emit('join', { userId: user._id });
    });
  }

  getRequests () {
    const { dispatch } = this.props;

    getRequests().then((requests) => {
      dispatch(setRequests(requests));
    });
  }

  initState () {
    this.setState({ participantLeavedRoom: false });
  }

  handleLoad () {
    const { dispatch } = this.props;

    Promise.all([
      getRequests(),
      getConversations()
    ]).then((values) => {
      const [requests, conversations] = values;

      const orderedRequests = orderBy(requests, ['desc']);

      dispatch(setRequests(requests));
      dispatch(setConversations(conversations));

      const activeRequest = orderedRequests[0];

      if (activeRequest) {
        if (activeRequest.isProcessing) {
          const activeConversation = conversations.find((item) => item.requests.includes(activeRequest.id));

          if (activeConversation) {
            dispatch(setActiveConversation(activeConversation));
            dispatch(setActiveRequest(activeRequest));
          }
        } else {
          dispatch(setActiveRequest(activeRequest));
        }
      }

      this.setState({ connected: true });
    });
  }

  render () {
    const { user } = this.props;
    const {
      participantLeavedRoom,
      languageLoaded,
      typing,
      connected
    } = this.state;

    if (!languageLoaded || (languageLoaded && (!user || (user && !connected)))) return null;

    return (
      <BrowserRouter basename="/">
        <Route render={router => <Header router={router} />} />

        <main id="app">
          <Route path="/login" render={router => <LoginPage router={router} />} />
          <Route path="/requests" render={router => <Requests router={router} />} />

          <Route path="/" render={() => <MainPage participantLeavedRoom={participantLeavedRoom} typing={typing} />} exact />
        </main>

        <RequestFrom />
      </BrowserRouter>
    );
  }
};

App.propTypes = {
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.object,
  isExit: PropTypes.bool.isRequired,
  conversation: PropTypes.object,
  conversations: PropTypes.arrayOf(PropTypes.object).isRequired,
  requests: PropTypes.arrayOf(PropTypes.object).isRequired
};

App.defaultProps = {
  conversation: null,
  user: null
};

const mapStateToProps = state => ({
  isExit: getIsExit(state),
  user: getCurrentUser(state),
  conversation: getActiveConversation(state),
  conversations: getConversationsProps(state),
  requests: getRequestsProps(state)
});

export default connect(mapStateToProps)(App);
