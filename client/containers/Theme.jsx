import React from 'react';
import PropTypes from 'prop-types';

import CssBaseline from '@material-ui/core/CssBaseline';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';

//  import GoogleAnalytics from 'Components/app/GoogleAnalytics';
import ReconnectionScreen from 'Components/app/ReconnectionScreen';

import theme from 'Styles/theme.js';

function Theme ({ children }) {
  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      {children}
      <ReconnectionScreen />
    </MuiThemeProvider>
  );
}

Theme.propTypes = {
  children: PropTypes.node.isRequired
};

export default Theme;
