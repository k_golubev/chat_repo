export default {
  initSettings: {
    state: '',
    options: {
      self: {
        gender: 'none',
        age: '-1'
      },
      searching: {
        gender: 'none',
        age: []
      }
    }
  }
};
