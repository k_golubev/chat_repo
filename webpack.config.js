const path = require('path');
const webpack = require('webpack');

const authConfig = require('./config/auth');

const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: {
    app: ['babel-polyfill', path.resolve('./client/app.jsx')]
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve('./public/build/dev/js'),
    chunkFilename: '[name].bundle.js',
    publicPath: '/build/dev/js/'
  },
  optimization: {
    minimizer: [new TerserPlugin({
      sourceMap: true
    })],
    runtimeChunk: {
      name: 'vendor'
    },
    splitChunks: {
      cacheGroups: {
        default: false,
        commons: {
          test: /node_modules/,
          name: 'vendor',
          chunks: 'initial',
          minChunks: 2,
          enforce: true
        }
      }
    }
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      Actions: path.resolve(__dirname, 'client/actions'),
      Containers: path.resolve(__dirname, 'client/containers'),
      Components: path.resolve(__dirname, 'client/components'),
      Modules: path.resolve(__dirname, 'client/modules'),
      Reducers: path.resolve(__dirname, 'client/reducers'),
      Rest: path.resolve(__dirname, 'client/rest'),
      Selectors: path.resolve(__dirname, 'client/selectors'),
      Styles: path.resolve(__dirname, 'client/styles'),
      Types: path.resolve(__dirname, 'client/types'),
      Data: path.resolve(__dirname, 'client/data')
    }
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        // This has effect on the react lib size
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        ORIGIN: JSON.stringify(`${authConfig.protocol}://${authConfig.host}`)
      }
    })
  ]
};
