require('../config/db');
var User = require('../server/models/user');

var emailAddress = process.argv[2];
var password = process.argv[3];
var role = process.argv[4];

if (emailAddress === undefined) {
  console.log('Please provide email.\n');
  process.exit();
}

if (password === undefined) {
  console.log('Please provide password\n');
  process.exit();
}

console.log('Creating user: ' + emailAddress + '\n');

var user = new User({
  email: emailAddress,
  password: {
    hash: User.prototype.generateHash(password)
  },
  name: '',
  role: role
});

user.save(function (err, result) {
  if (err) {
    console.log(err);
  }

  console.log('Done');
  process.exit();
});
