const async = require('async');

require('../config/db');

const Language = require('../server/models/language');

const en = new Language({
  title: 'English',
  key: 'en'
});

const ru = new Language({
  title: 'Русский',
  key: 'ru'
});

const de = new Language({
  title: 'Deutsch',
  key: 'de'
});

const es = new Language({
  title: 'Spanish',
  key: 'es'
});

const zh = new Language({
  title: '中国',
  key: 'zh'
});

const pt = new Language({
  title: 'Português',
  key: 'pt'
});

const fr = new Language({
  title: 'Français',
  key: 'fr'
});

async.parallel([
  function (callback) {
    Language.deleteMany({}, callback);
  },
  function (callback) {
    en.save(callback);
  },
  function (callback) {
    ru.save(callback);
  },
  function (callback) {
    de.save(callback);
  },
  function (callback) {
    es.save(callback);
  },
  function (callback) {
    zh.save(callback);
  },
  function (callback) {
    pt.save(callback);
  },
  function (callback) {
    fr.save(callback);
  }
], function () {
  console.log('Languages are added\n');
  process.exit();
});
