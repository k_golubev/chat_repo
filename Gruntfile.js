const path = require('path');

const webpackConfig = require('./webpack.config.js');

const publicDirectory = path.resolve(process.cwd(), 'public');
const buildDirectory = path.resolve(publicDirectory, 'build');

const configureGrunt = function (grunt) {
  // load all grunt tasks
  require('matchdep').filter('grunt-*').forEach(grunt.loadNpmTasks);

  const config = {
    pkg: grunt.file.readJSON('package.json'),
    paths: {
      publicDirectory,
      devBuild: path.join(buildDirectory, 'dev'),
      prodBuild: path.join(buildDirectory, 'prod')
    },
    filesToLint: [
      'Gruntfile.js',
      'server.js'
    ],
    express: {
      dev: {
        options: {
          script: 'server.js',
          node_env: 'development'
        }
      },
      prod: {
        options: {
          script: 'server.js',
          node_env: 'production'
        }
      }
    },
    watch: {
      options: {
        livereload: true
      },
      jsx: {
        files: ['client/**/*.jsx', 'client/**/*.js'],
        tasks: ['webpack:dev']
      },
      express: {
        // Restart any time client or server js files change
        files: [
          'server.js',
          'server/**/*.js',
          'server/**/*.json',
          'config/**/*.js'
        ],
        tasks: ['express:dev'],
        options: {
          // Without this option specified express won't be reloaded
          spawn: false
        }
      }
    },
    mkdir: {
      all: {
        options: {
          mode: parseInt('0700', 8),
          create: [
            'tmp'
          ]
        }
      },
      prod: {
        options: {
          mode: parseInt('0700', 8),
          create: [
            'build',
            '<%= paths.prodBuild %>',
            '<%= paths.prodBuild %>/js'
          ]
        }
      }
    },
    clean: {
      options: {
        force: true
      },
      dev: ['<%= paths.devBuild %>'],
      prod: ['<%= paths.prodBuild %>', 'build']
    },
    webpack: {
      options: {
        stats: !process.env.NODE_ENV || process.env.NODE_ENV === 'development'
      },
      prod: () => {
        const config = Object.assign({}, webpackConfig, { mode: 'production' });
        config.output.path = path.resolve('./public/build/prod/js');
        config.optimization.minimize = true;
        return config;
      },
      dev: webpackConfig
    }
  };

  grunt.initConfig(config);
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('build:prod', [
    'clean:prod',
    'mkdir:prod',
    'build:dev',
    'webpack:prod'
  ]);

  grunt.registerTask('build:dev',
    'Build the development files', [
      'clean:dev',
      'mkdir:all',
      'webpack:dev'
    ]
  );

  grunt.registerTask('dev', [
    'build:dev',
    'express:dev',
    'watch'
  ]);

  // Default task(s).
  grunt.registerTask('default', 'dev');
};

module.exports = configureGrunt;
