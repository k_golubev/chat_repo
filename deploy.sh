#!/bin/bash

# aws - pipeline on codecommit
# origin - pipeline on bitbucket

if [ "$#" -eq 0 ]; then
  echo "Environment is required."
  echo "Usage: ./deploy.sh [dev|prod]"
  exit 1;
fi

branchName=$2

if [ "$1" == "prod" ]; then
  branchName="PRODUCTION"
fi

git pull
git checkout $branchName
git push aws $branchName

if [ "$1" == "dev" ]; then
  echo "switching to dev-elb..."
  eb use chatv2-env
	echo "deploying to dev"
elif [ "$1" == "prod" ]; then
  echo "switching to prod..."
  eb use chatv2-prod
	echo "deploying to prod"
fi

eb deploy --source="codecommit/chat_repo/$branchName"

git checkout $branchName
