const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Request = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  settings: {
    state: {
      type: String,
      default: ''
    },
    options: {
      self: {
        gender: {
          type: String,
          default: 'none',
          enum: [
            'none',
            'male',
            'female'
          ]
        },
        age: {
          type: String,
          default: '-1',
          enum: [
            '-1',
            '0',
            '1',
            '2',
            '3'
          ]
        }
      },
      searching: {
        gender: {
          type: String,
          default: 'none',
          enum: [
            'none',
            'male',
            'female'
          ]
        },
        age: [String]
      }
    }
  },
  status: {
    type: String,
    default: 'pending',
    enum: [
      'pending',
      'processing',
      'completed',
      'expired'
    ]
  }
}, {
  timestamps: true
});

Request.virtual('isPending').get(function () {
  return this.status === 'pending';
});

Request.virtual('isCompleted').get(function () {
  return this.status === 'completed';
});

Request.virtual('isExpired').get(function () {
  return this.status === 'expired';
});

Request.virtual('isProcessing').get(function () {
  return this.status === 'processing';
});

Request.set('toJSON', {
  virtuals: true
});

module.exports = mongoose.model('Request', Request);
