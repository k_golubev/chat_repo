var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Language = new Schema({
  title: {
    type: String
  },
  key: {
    type: String,
    unique: true
  }
}, {
  timestamps: true
});

Language.set('toJSON', {
  virtuals: true
});

module.exports = mongoose.model('Language', Language);
