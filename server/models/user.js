const mongoose = require('mongoose');
//  const validator = require('validator');
const bcrypt = require('bcrypt-nodejs');

const Logger = require('../../config/logger');

const Schema = mongoose.Schema;

const User = new Schema({
  socketId: {
    type: String,
    unique: false
  },

  email: {
    type: String,
    default: ''
  },

  name: {
    type: String,
    default: ''
  },

  password: {
    select: false,
    type: {
      hash: String,
      resetToken: String,
      resetExpires: Date
    }
  },

  role: {
    type: String,
    default: ''
  }
}, {
  timestamps: true
});

User.virtual('isSU').get(function () {
  return this.role && this.role === 'su';
});

// generating a hash
User.methods.generateHash = function (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
User.methods.validPassword = function (password) {
  try {
    return bcrypt.compareSync(password, this.password.hash);
  } catch (err) {
    Logger.warn(`Error in validating the password: ${err}`);

    return false;
  }
};

User.set('toJSON', {
  virtuals: true
});

module.exports = mongoose.model('User', User);
