const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Conversation = new Schema({
  participants: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  states: [{
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    state: {
      type: String,
      default: ''
    }
  }],
  requests: [{
    type: Schema.Types.ObjectId,
    ref: 'Request'
  }],
  messages: [{
    from: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    body: {
      type: String
    },
    createdAt: {
      type: Date,
      default: new Date()
    }
  }]
}, {
  timestamps: true
});

module.exports = mongoose.model('Conversation', Conversation);
