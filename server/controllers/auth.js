const User = require('../models/user');

module.exports = (passport) => {
  return {
    all: (req, res, next) => {
      next();
    },

    index: async (req, res) => {
      let userMustCreate = false;

      if (!req.session.user) {
        userMustCreate = true;
      } else {
        const user = await User.findOne({ _id: req.session.user._id });

        if (!user) userMustCreate = true;
      }

      if (userMustCreate) {
        const user = new User();
        await user.save();

        req.session.user = user;
      }

      res.render('index');
    },

    login: (req, res, next) => {
      passport.authenticate('local', (err, user) => {
        if (err) {
          return next(err);
        }

        if (!user) {
          res.status(400).json({ message: 'User not found' });

          return;
        }

        req.logout();

        req.login(user, (err) => {
          if (err) return next(err);

          req.session.user = user;

          res.status(200).json({ redirectUrl: '/' });
        });
      })(req, res, next);
    },

    logout: (req, res) => {
      req.logout();

      req.session.destroy((err) => {
        if (!err) res.redirect('/');
      });
    }
  };
};
