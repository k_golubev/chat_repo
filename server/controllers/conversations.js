const Conversation = require('../models/conversation');
const Request = require('../models/request');

module.exports = {
  all: (req, res, next) => {
    next();
  },

  getAll: async (req, res, next) => {
    try {
      const condition = req.session.user.isSU ? {} : { user: req.session.user };
      const requests = await Request.find(condition);

      const conversations = await Conversation.find({ requests: { $in: requests }, participants: req.session.user });

      res.status(200).json(conversations);
    } catch (err) {
      console.log(err);
      next(err);
    }
  },

  getOne: async (req, res, next) => {
    try {
      const conversationtId = req.params.id;

      const request = await Conversation.findById(conversationtId);

      res.status(200).json(request);
    } catch (err) {
      next(err);
    }
  }
};
