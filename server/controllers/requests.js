const multiparty = require('multiparty');
const path = require('path');
const fs = require('fs');
const util = require('util');
const csv = require('csv');
const escape = require('lodash/escape');

const Conversation = require('../models/conversation');
const Request = require('../models/request');
const User = require('../models/user');

module.exports = (io) => {
  const promisifyUpload = (req) => new Promise((resolve, reject) => {
    const form = new multiparty.Form();

    form.parse(req, function (err, fields, files) {
      if (err || !files.csv) return reject(err || 'No file was sent');

      const csvFileObj = files.csv[0];
      const ext = path.extname(csvFileObj.path);

      if (ext.toLowerCase() !== '.csv') {
        const err = 'Unsupported file type';

        return reject(err);
      }

      return resolve(csvFileObj);
    });
  });

  const csvPromise = (fileData) => new Promise((resolve, reject) => {
    csv.parse(fileData.toString(), { delimiter: '\n' }, (err, rows) => {
      if (err) return reject(err);

      return resolve(rows);
    });
  });

  const readFile = (fileName) => util.promisify(fs.readFile)(fileName, 'utf8');

  return {
    all: (req, res, next) => {
      next();
    },

    getAll: async (req, res, next) => {
      try {
        const condition = req.session.user.isSU ? {} : { user: req.session.user };

        const requests = await Request.find(condition);

        res.status(200).json(requests);
      } catch (err) {
        console.log(err);
        next(err);
      }
    },

    getOne: async (req, res, next) => {
      try {
        const requestId = req.params.id;

        const request = await Request.findById(requestId);

        res.status(200).json(request);
      } catch (err) {
        next(err);
      }
    },

    create: async (req, res, next) => {
      try {
        let user = req.session.user;
        user = user ? await User.findById(user._id) : null;

        const settings = req.body.settings;

        if (!user) {
          user = new User();
          await user.save();
        }

        req.session.user = user;

        if (!user.isSU) await Request.deleteMany({ user });

        const request = new Request({ settings, user });
        await request.save();

        res.status(200).json({ request, user });
      } catch (err) {
        next(err);
      }
    },

    update: async (req, res, next) => {
      try {
        const requestId = req.params.id;
        const user = req.session.user;

        const nextRequest = {
          settings: req.body.settings,
          status: req.body.status
        };

        if (nextRequest.status === 'pending' && req.body.conversation) {
          const conversation = await Conversation.findById(req.body.conversation._id);

          if (conversation) {
            conversation.participants.forEach(async () => {
              io.to(conversation._id).emit('participan-leave-conversation', { userId: user._id, conversationId: conversation._id });
            });
          }
        }

        await Request.findByIdAndUpdate(requestId, nextRequest, { new: true });
        const request = await Request.findOne({ _id: requestId });

        res.status(200).json(request);
      } catch (err) {
        next(err);
      }
    },

    delete: async (req, res, next) => {
      try {
        const requestId = req.params.id;
        const user = req.session.user;

        const request = await Request.findOne({ _id: requestId });

        const conversations = await Conversation.find({ requests: request }).populate('requests');

        conversations.forEach((conversation) => {
          conversation.participants.forEach(async () => {
            io.to(conversation._id).emit('participan-leave-conversation', { userId: user._id, conversationId: conversation._id });
          });
        });

        await Request.deleteOne({ _id: requestId });

        res.status(200).json({});
      } catch (err) {
        next(err);
      }
    },

    getConversation: async (req, res, next) => {
      try {
        const requestId = req.params.id;

        const conversations = await Conversation.find({ requests: requestId, participants: req.session.user });

        res.status(200).json(conversations);
      } catch (err) {
        next(err);
      }
    },

    importCSV: async (req, res, next) => {
      try {
        const user = req.session.user;

        const csvFileObj = await promisifyUpload(req);
        const fileData = await readFile(csvFileObj.path);
        const rows = await csvPromise(fileData);

        const requests = [];

        rows.forEach((item) => {
          const row = item[0].split(/,/g);

          const state = escape(row[0]) || '';
          const self_gender = row[1] || 'none';
          const sefl_age = row[2] || -1;

          const searching_gender = row[3] || 'none';
          const searching_ages = [];

          if (row[4] && row[4] === '1') searching_ages.push(0);
          if (row[5] && row[5] === '1') searching_ages.push(1);
          if (row[6] && row[6] === '1') searching_ages.push(2);
          if (row[7] && row[7] === '1') searching_ages.push(3);

          const settings = {
            state,
            options: {
              self: {
                gender: self_gender,
                age: sefl_age
              },
              searching: {
                gender: searching_gender,
                age: searching_ages
              }
            }
          };

          requests.push({ settings, user });
        });

        Request.insertMany(requests).then((docs) => {
          res.status(200).json(docs);
        }).catch(function (err) {
          res.status(500).send(err);
        });
      } catch (err) {
        next(err);
      }
    },

    removeAll: async (req, res, next) => {
      try {
        const isSU = req.session.user && req.session.user.role === 'su';

        if (!isSU) {
          res.sendStatus(400);

          return;
        }

        await Request.deleteMany({});
        await Conversation.deleteMany({});

        res.status(200).json({});
      } catch (err) {
        next(err);
      }
    }
  };
};
