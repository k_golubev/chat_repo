const Conversation = require('../models/conversation');
const User = require('../models/user');

module.exports = {
  start: async (req, res, next) => {
    try {
      let user = req.session.user;
      user = user ? await User.findById(user._id) : null;

      const status = 'searching';
      const settings = req.body.settings;

      if (user) {
        user = await User.findByIdAndUpdate(user._id, { settings, status }, { new: true });
      } else {
        user = new User({ settings, status });
        await user.save();
      }

      req.session.user = user;

      res.status(200).json(user);
    } catch (err) {
      next(err);
    }
  },

  message: async (req, res, next, io) => {
    try {
      const conversationId = req.body.conversation;
      const message = req.body.message;

      const conversation = await Conversation.findByIdAndUpdate(conversationId, { $addToSet: { messages: message } }, { new: true });

      if (conversation) io.to(conversation._id).emit('update-conversation', conversation);

      req.session.save();

      res.status(200).json({});
    } catch (err) {
      next(err);
    }
  }
};
