const path = require('path');

const Language = require('../models/language.js');

const i18nDir = '../helpers/i18n';

exports.all = (req, res, next) => {
  next();
};

exports.getAll = async (req, res, next) => {
  try {
    const languages = await Language.find({});

    res.status(200).json(languages);
  } catch (err) {
    next(err);
  }
};

exports.getOne = async (req, res, next) => {
  try {
    const key = req.params.key;

    const language = await Language.findOne({ key });
    const vocabulary = language ? require(path.join(i18nDir, `${language.key}.json`)) : {};

    res.status(200).json(vocabulary);
  } catch (err) {
    next(err);
  }
};
