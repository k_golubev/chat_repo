const User = require('../models/user');

module.exports = {
  all: (req, res, next) => {
    next();
  },

  getCurrentUser: async (req, res, next) => {
    try {
      let user = req.session.user;

      if (!user) {
        user = new User();
        await user.save();
      } else {
        user = await User.findOne({ _id: user._id });

        if (!user) {
          user = new User();
          await user.save();
        }
      }

      req.session.user = user;

      res.status(200).json(user);
    } catch (err) {
      next(err);
    }
  }
};
