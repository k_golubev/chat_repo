const express = require('express');
const router = express.Router();

const controller = require('../controllers/conversations');

module.exports = (app) => {
  router.all('*', controller.all);
  router.get('/', controller.getAll);
  router.get('/:id', controller.getOne);

  app.use('/app/conversations', router);
};
