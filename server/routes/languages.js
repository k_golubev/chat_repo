const express = require('express');
const router = express.Router();

const controller = require('../controllers/languages');

module.exports = function (app) {
  router.all('*', controller.all);
  router.get('/', controller.getAll);
  router.get('/:key', controller.getOne);

  app.use('/locales', router);
};
