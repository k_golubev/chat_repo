const express = require('express');
const router = express.Router();

const helpers = require('../helpers/users');

module.exports = function (app, io, passport) {
  const auth = require('../controllers/auth')(passport);
  router.all('*', auth.all);

  router.get('/', auth.index);
  router.get('/login', auth.index);
  router.get('/logout', auth.logout);
  router.get('/requests', helpers.requiresLogin, auth.index);

  router.post('/login', auth.login);

  app.use('/', router);
};
