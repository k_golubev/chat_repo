const express = require('express');
const router = express.Router();

const controller = require('../controllers/users');

module.exports = (app) => {
  router.all('*', controller.all);
  router.get('/current', controller.getCurrentUser);

  app.use('/users', router);
};
