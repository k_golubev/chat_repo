const express = require('express');
const router = express.Router();

module.exports = (app, io) => {
  const controller = require('../controllers/requests')(io);

  router.all('*', controller.all);
  router.get('/', controller.getAll);
  router.get('/:id', controller.getOne);
  router.get('/:id/conversation', controller.getConversation);
  router.post('/', controller.create);
  router.put('/:id', controller.update);
  router.delete('/:id', controller.delete);
  router.post('/import', controller.importCSV);
  router.post('/clear', controller.removeAll);

  app.use('/app/requests', router);
};
