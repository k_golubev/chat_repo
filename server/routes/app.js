const express = require('express');
const router = express.Router();

const controller = require('../controllers/app');

module.exports = (app, io) => {
  router.post('/start', controller.start);
  router.post('/message', (...props) => controller.message(...props, io));

  app.use('/app', router);
};
